<?php

class Home extends CI_Controller
{

  public function __construct()
  {

    parent::__construct();

    $this->load->model('homeModel');
    $this->load->model('staffModel');

  }

  function templates($path, $data)
  {

    if ($this->session->userdata('is_logged_in') == 1) {
      $this->load->view('templates/header', $data);
      $this->load->view($path, $data);
      $this->load->view('templates/footer');
    } else {
      $this->load->view('templates/header', $data);
      $this->load->view('restricted/restricted', $data);
      $this->load->view('templates/footer');
    }

  }

  function index()
  {

    $data['title'] = "Records | Admin";
    $data['table'] = $this->homeModel->getData();

    $this->templates('table', $data);

  }

  function add()
  {

    $data = array(
      'title' => "Add New Record",
      'category' => $this->homeModel->getCategory(),
      'shelve' => $this->homeModel->getShelvePosition()
    );

    $this->templates('actions/addRecords', $data);

  }

  function save()
  {

    $upload_path = realpath(APPPATH . '../fileuploads');

    $config = array(
      'upload_path' => $upload_path,
      'allowed_types' => 'pdf|doc|docx',
      'max_size' => '0'
    );

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('uploadFile')) {
      $this->homeModel->insertRecordsFailed();
      redirect('home/index');
    } else {
      $this->homeModel->insertRecords($this->upload->data());
      redirect('home/index');
    }

  }

  function edit($record_id)
  {

    $data = array(
      'title' => "Edit Record",
      'records' => $this->homeModel->getEditRecords($record_id),
      'category' => $this->homeModel->getCategory(),
      'shelve' => $this->homeModel->getShelvePosition()
    );

    $this->templates('actions/editRecords', $data);

  }

  function view($record_id)
  {

    $data = array(
      'title' => "View Records",
      'viewRec' => $this->homeModel->getDataToView($record_id)
    );

    $this->templates('actions/viewRecords', $data);

  }

  function update()
  {

    $id = $this->uri->segment(3);
    $upload_path = realpath(APPPATH . '../fileuploads');

    $config = array(
      'upload_path' => $upload_path,
      'allowed_types' => 'pdf|doc|docx',
      'overwrite' => true,
      'max_size' => '0'
    );

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('editUploadFile')) {

      $this->homeModel->updateRecFailed();

      redirect('home/index');

    } else {

      $query = $this->get_records('records_id', $id, 'records');

      if ($query->num_rows() > 0) {
        foreach ($query->result() as $file) {
          $this->db->where('records_id', $id);
          unlink($file->filePath);
        }
      }

      $this->homeModel->updateRec($this->upload->data());
      redirect('home/index');

    }

  }

  function delete($record_id)
  {

    $query = $this->get_records('records_id', $record_id, 'records');

    if ($query->num_rows() > 0) {
      foreach ($query->result() as $key) {
        $this->_delete('records_id', $record_id, 'records');
        unlink($key->filePath);
      }
    }

    redirect('home/index');

  }

  function download($record_id)
  {

    $query = $this->get_records('records_id', $record_id, 'records');

    if ($query->result() > 0) {
      foreach ($query->result() as $key) {
        force_download($key->filePath, null);
      }
    }

    return false;

  }

  function viewPDF($record_id)
  {

    $query = $this->get_records('records_id', $record_id, 'records');

    if ($query->result() > 0) {
      foreach ($query->result() as $key) {
        $fp = fopen($key->filePath, "r");
        header("Cache-Control: maxage=1");
        header("Pragma: public");
        header("Content-type: application/pdf");
        header("Content-Disposition: inline; filename=" . $key->fileName . "");
        header("Content-Description: PHP Generated Data");
        header("Content-Transfer-Encoding: binary");
        header('Content-Length:' . filesize($key->filePath));
        ob_clean();
        flush();
        while (!feof($fp)) {
          $buff = fread($fp, 1024);
          print $buff;
        }
        exit;
      }
    }

    return false;

  }

  function get_records($where, $id, $table)
  {

    $this->db->where($where, $id);
    return $this->db->get($table);

  }

  function _delete($where, $id, $table)
  {

    $this->db->where($where, $id);
    return $this->db->delete($table);

  }
}

