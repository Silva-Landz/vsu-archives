<?php

  class Login extends CI_Controller  {

      function __construct()  {

        parent::__construct();
        $this->load->model('login_users');
        $this->load->library('form_validation');

      }

      function templates($path, $data)  {

        $this->load->view('templates/header', $data);
        $this->load->view($path);

      }

      function index()  {

        $data = array(
          'title' => "Login | Records Office and Archives Center"
        );

        $this->templates('login/user_login', $data);

      }

      function login_validation() {

        $this->form_validation->set_rules('txtEmail', 'username', 'required|trim|callback_validate_credentials');
        $this->form_validation->set_rules('txtPass', 'password', 'required|md5|trim');

        if ($this->form_validation->run() == FALSE) {

          $data['title'] = "Login";

          $this->templates('login/user_login', $data);
         
        } else {

          $data['username'] = $this->input->post('txtEmail');
          $data['is_logged_in'] = 1;

          $this->session->set_userdata($data);
          
          redirect('home/index', 'refresh');
        }

      }

      function validate_credentials() {

        if ($this->login_users->can_login()) {
          return true;
        }
        else {
          $this->form_validation->set_message('validate_credentials', 'Incorrect username/password.');
          return false;
        }

      }

      function register() {

        $data['title'] = "Register Your Account";

        $this->form_validation->set_rules('txtEmail', 'username', 'required|trim');
        $this->form_validation->set_rules('txtPass', 'password', 'required|trim');
        $this->form_validation->set_rules('txtConfirmPass', 'confirm password', 'required|trim|matches[txtPass]');

        if($this->form_validation->run() == FALSE)  {

          $this->templates('login/register_account', $data);

        } else {

          if($this->login_users->register_account())  {

            $message = "<p class='alert alert-success'>Successfully created an account</p>";

          } else {

            $message = "<p class='alert alert-danger'>Failed to created an account</p>";

          }

          $this->session->set_flashdata('message', $message);

          redirect('login', 'refresh');

        }

      }

      function logout() {

        $this->session->sess_destroy();
        redirect('homepage/index');

      }

      function reset_password() {

        $data = array(
            'title' => "Reset Password"
          );

        $this->templates('login/reset_pass', $data);
        
      }
  }

?>
