<?php

  class About extends CI_Controller {

    function __construct()  {

      parent::__construct();
      $this->load->model('staffModel');

    }

    function templates($path, $data) {

      $this->load->view('templates/header', $data);
      $this->load->view($path, $data);
      $this->load->view('templates/footer');

    }

    function faculties() {

      if ($this->session->userdata('is_logged_in')) {
        $data = array(
          'title'    => "About | Faculties",
          'staffs'   => $this->staffModel->getStaff(), 
          'position' => $this->staffModel->getPosition()
        );

        $this->templates('about/about', $data);
      }
      else {
        redirect('home/restricted');
      }

    }

    function getSelectedStaff($id) {

      if ($this->session->userdata('is_logged_in')) {
        $data = array(
          'title'          => "About | Edit Staff",
          'selectedStaffs' => $this->staffModel->getSelectedStaff($id),
          'position'       => $this->staffModel->getPosition()
        );

        $this->templates('actions/editStaff', $data);
      }
      else {
        redirect('home/restricted');
      }

    }

    function addEmployee() {

      $upload_path = realpath(APPPATH . '../img/faculties');

      if ($this->session->userdata('is_logged_in')) {
        $config = array(
          'upload_path'   => $upload_path, 
          'allowed_types' => 'jpeg|jpg|png|tif|gif|bmp',
          'max_size'      => '0'
        );

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('fileProfImg'))  {
          $this->staffModel->addEmployeeFailed();
          redirect('about/faculties');
        }
        else {
          $this->staffModel->addEmployee($this->upload->data());
          redirect('about/faculties');
        }
      }
      else {
        redirect('home/restricted');
      }

    }

    function updateEmployee() {

      $id = $this->uri->segment(3);
      $upload_path = realpath(APPPATH . '../img/faculties');

      if ($this->session->userdata('is_logged_in')) {
        $config = array(
          'upload_path'   => $upload_path,
          'overwrite'     => true,
          'allowed_types' => 'jpeg|jpg|png|tif|gif|bmp',
          'max_size'      => '0'
        );

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('imgEditFile'))  {

          $this->staffModel->addEmployeeFailed();
          redirect('about/faculties');

        }
        else {

          $query = $this->_getimage('staff_id', $id, 'staff');

          if ($query->num_rows() > 0) {
            foreach ($query->result() as $img) {
              $this->db->where('staff_id', $id);
              unlink($upload_path. "\\" .$img->filename); 
            }
          }

          $this->staffModel->updateEmployee($this->upload->data());
          redirect('about/faculties');

        }
      }
      else {
        redirect('home/restricted');
      }

    }

    function deleteSelectedStaff($id)  {
      
      $upload_path = realpath(APPPATH . '../img/faculties');
      $query = $this->_getimage('staff_id', $id, 'staff');
      
      if ($query->num_rows() > 0) {
        foreach ($query->result() as $img) {
          $this->_delete('staff_id', $id, 'staff');
          unlink($upload_path. "\\" .$img->filename);
        }
      }

      redirect('about/faculties');
    
    }

    function _getimage($where, $id, $table)  {

      $this->db->where($where, $id);
      return $this->db->get($table);

    }

    function _delete($where, $id, $table)  {

      $this->db->where($where, $id);
      return $this->db->delete($table);

    }
    
  }

