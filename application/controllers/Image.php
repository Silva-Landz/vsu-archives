<?php

  class Image extends CI_Controller {

      function __construct()  {

        parent::__construct();
        $this->load->model('imageModel');
        
      }

      function templates($path, $data)  {

        $this->load->view("templates/header", $data);
        $this->load->view($path, $data);
        $this->load->view("templates/footer");

      }

      function image_gallery()  {

        if ($this->session->userdata('is_logged_in')) {
          $data = array(
            'title'  => "Gallery",
            'tags'   => $this->imageModel->getTags(),
            'images' => $this->imageModel->projectImage()
          );

          $this->templates('images/gallery', $data);
        }
        else {
            redirect('home/restricted');
        }

      }

      function insertImg()  {

        $upload_path = realpath(APPPATH . '../img/archives');

        if ($this->session->userdata('is_logged_in')) {
          $config = array(
            'upload_path'   => $upload_path,
            'allowed_types' => 'jpeg|jpg|png|tif|gif|bmp', 
            'max_size'      => '0'
          );

          $this->load->library('upload', $config);

          if(!$this->upload->do_upload('fileImg'))  {
            $this->imageModel->insertImgFailed();
            redirect('image/image_gallery');
          }
          else {
            $this->imageModel->insertImg($this->upload->data());
            redirect('image/image_gallery');
          }
        }
        else {
          redirect('home/restricted');
        }

      }

      function getSelectedImage($id) {

        $data = array(
          'title'   => "Edit Image",
          'images'  => $this->imageModel->getSelectedImage($id),
          'imgTags' => $this->imageModel->getTags() 
        );

        $this->templates('actions/editImage', $data);

      }

      function updateImage()  {

        $id = $this->uri->segment(3);
        $upload_path = realpath(APPPATH . '../img/archives');

        if ($this->session->userdata('is_logged_in')) {
          $config = array(
            'upload_path'   => $upload_path,
            'allowed_types' => 'jpeg|jpg|png|tif|gif|bmp',
            'max_size'      => '0'
          );

          $this->load->library('upload', $config);

          if(!$this->upload->do_upload('editImgFile'))  {

            $this->imageModel->updateImageFailed();
            redirect('image/image_gallery');

          }
          else {

            $query = $this->_getimage('picGallery_id', $id, 'picgallery');

            if ($query->num_rows() > 0) {
              foreach ($query->result() as $img) {
                $this->db->where('picGallery_id', $id);
                unlink($img->filepath);
              }
            }
          
            $this->imageModel->updateImageSuccessful($this->upload->data());
            redirect('image/image_gallery');

          }
        }
        else {
          redirect('home/restricted');
        }

      }

      function deleteSelectedImage($id)  {

        $query = $this->_getimage('picGallery_id', $id, 'picgallery');

        if($query->num_rows() > 0)  {
          foreach ($query->result() as $key) {
            $this->_deleteimage('picGallery_id', $id, 'picgallery');
            unlink($key->filepath);
          }
        }
        redirect('image/image_gallery');
        
      }

      function _getimage($where, $id, $table) {

        $this->db->where($where, $id);
        return $this->db->get($table);

      }

      function _deleteimage($where, $id, $table) {

        $this->db->where($where, $id);
        return $this->db->delete($table);

      }
  }

?>
