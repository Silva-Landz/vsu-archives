<?php

  class Homepage extends CI_Controller  {

    function __construct()  {

      parent::__construct();

      $this->load->model('homeModel'); //loads the home model.
      $this->load->model('staffModel'); //loads the staff model
      $this->load->model('imageModel'); //loads the image model

    }

    function templates($path, $data) {

      $this->load->view('templates/header', $data);
      $this->load->view($path, $data);
      $this->load->view('templates/footer');

    }

    function index()  {

      $data = array(
        'title' => "Home | Records Office and Archives Center" 
      );

      $this->templates('home/homepage', $data);

    }

    function gallery()  {

      $data = array(
        'title'  => "Gallery | Records Office and Archives Center",
        'tags'   => $this->imageModel->getTags(),
        'images' => $this->imageModel->getAllImage() 
      );

      $this->templates('home/homegallery', $data);

    }

    function downloads()  {

      $this->load->library('pagination');

      $config = array(
        'base_url'        =>  base_url(). 'homepage/downloads',
        'total_rows'      =>  count($this->homeModel->getData()),
        'per_page'        =>  20,
        'uri_segment'     =>  3,

        'full_tag_open'   => '<ul class="pagination">',
        'full_tag_close'  =>  '</ul>',

        'first_link'      =>  '&laquo; First',
        'first_tag_open'  =>  '<li class="page-item">',
        'first_tag_close' =>  '</li>',

        'last_link'       =>  'Last &raquo;',
        'last_tag_open'   =>  '<li class="page-item">',
        'last_tag_close'  =>  '</li>',

        'next_link'       =>  'Next &rarr;',
        'next_tag_open'   =>  '<li class="next page">',
        'next_tag_close'  =>  '</li>',

        'prev_link'       =>  '&larr; Previous',
        'prev_tag_open'   =>  '<li class="prev page">',
        'prev_tag_close'  =>  '</li>',

        'cur_tag_open'    =>  '<li class="active"><a href="">',
        'cur_tag_close'   =>  '</a></li>',

        'num_tag_open'    =>  '<li class="page">',
        'num_tag_close'   =>  '</li>'
      );

      $this->pagination->initialize($config);

      $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      $keyword = $this->input->post('txtSearchRecord');

      $data = array(
        'title' => "Downloads | Records Office and Archives Center", 
        'table' => $this->homeModel->getPaginateData($config['per_page'], $page, $keyword),
        'links' => $this->pagination->create_links()
      );

      $this->templates('home/homedownloads', $data);

    }

    function about()  {

      $data = array(
        'title'   => "About | Records Office and Archives Center",
        'staffs'  => $this->staffModel->getStaff()
      );

      $this->templates('home/homeabout', $data);

    }

    function contacts()  {

      $data = array(
        'title' => "Contacts | Records Office and Archives Center" 
      );

      $this->templates('home/homecontacts', $data);

    }
    
  }

?>
