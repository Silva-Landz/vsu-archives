  <!--Navbar-->
  <nav class="navbar navbar-dark bg-primary">
    <!-- Collapse button-->
    <button class="navbar-toggler hidden-sm-up btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseEx2" style="background:transparent;border:0px;margin-top:8px;">
      <i class="material-icons">&#xE5D2;</i>
    </button>

    <a href="<?php echo base_url('login/logout'); ?>" class="btn-sm btn-primary pull-right" style="margin-top:10px;margin-right:10px;background:transparent" title="logout"><span class="fa fa-sign-out"></span>Logout</a>

    <div class="container">
      <!--Collapse content-->
      <div class="collapse navbar-toggleable-xs" id="collapseEx2">
        <!--Links-->
        <ul class="nav navbar-nav">
          <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>"><b>Records Office</b></a></li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('home/index'); ?>">Archives</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url('image/image_gallery'); ?>">Gallery <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('about/faculties'); ?>">About</a>
          </li>
        </ul>
        <!-- End Links -->
      </div>
      <!--/.Collapse content-->
    </div>
  </nav>
  <!--/.Navbar-->

  <div id="table" class="container">
    <div class="row">
      <h3><b>Gallery</b></h3>
      <hr>
      <div class="col-sm-6">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal7" title="Add image">ADD IMAGE</button>
      </div>
      <div class="col-sm-4 col-sm-offset-2">
        <input type="text" class="search" placeholder="Search image">
      </div>
    </div>

    <div class="row">
      <!-- Modal -->
      <div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
        <div class="modal-dialog " role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #0B791C; color: white">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <p class="modal-title">Add Image</p>
            </div>
            <!-- FORM HERE -->
            <form action="<?php echo base_url(). 'image/insertImg'; ?>" method="post" enctype="multipart/form-data">
              <div class="modal-body" style="padding-left:30px; padding-right:30px">
                <div class="form-group">
                  <label>Title</label>
                  <input type="text" name="txtTitle" class="form-control" placeholder="Enter title" required>
                </div>
                <div class="form-group">
                  <label>Description</label>
                  <textarea name="txtDescription" class="form-control" rows="5" cols="80" placeholder="Enter description" style="border-radius:0;"></textarea>
                </div>
                <div class="form-group">
                  <label>Tags</label>
                  <select class="form-control" name="slctTag" required>
                    <option value="">Choose...</option>
                    <?php foreach ($tags as $tags): ?>
                      <option value="<?php echo $tags->tags_id; ?>"><?php echo $tags->tags; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Upload image</label>
                  <input type="file" class="form-control" name="fileImg">
                </div>
              </div>
              <div class="form-group">
                <div class="modal-footer">
                  <button type="button" name="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <span><button type="submit" class="btn btn-primary">Save Changes</button></span>
                </div>
              </div>
            </form>
            <!-- END FORM -->
          </div>
        </div>
      </div>
    </div><br>

    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-responsive">
          <a href="#" id="return-to-top" style="z-index:1; "><i class="fa fa-chevron-up"></i></a>
            <thead style="background-color:#25ACD6; color: white; ">
              <tr>
                <th class="col-sm-1 text-center">ACTIONS</th>
                <th class="col-sm-2 text-center">IMAGE</th>
                <th class="sort col-sm-4 text-center" data-sort="IMGTITLE">TITLE</th>
                <th class="sort col-sm-5 text-center" data-sort="IMGDESC">DESCRIPTION</th>
              </tr>
            </thead>
            <tbody class="list">
              <?php foreach ($images as $img): ?>
                <tr>
                  <td class="text-center">
                    <a class="btn btn-info btn-sm" href="<?php echo base_url('image/getSelectedImage/'). $img->picGallery_id; ?>" title="Edit image">Edit</a>
                    <a class="btn btn-danger btn-sm" href="<?php echo base_url('image/deleteSelectedImage/'). $img->picGallery_id; ?>" onclick="return deleteConfirm();" title="Remove image">Delete</a>
                  </td>
                  <td><img src="<?php echo base_url(). 'img/archives/'. $img->filename; ?>" alt="<?php echo $img->filename?>" width="150" height="150" class="img-responsive  z-depth-1"></td>
                  <?php if ($img->picGallery_title == null): ?>
                    <td class="IMGTITLE"><?php echo "No title"; ?></td>
                  <?php else: ?>
                    <td class="IMGTITLE"><?php echo $img->picGallery_title; ?></td>
                  <?php endif; ?>

                  <?php if ($img->picGallery_desc == null): ?>
                    <td class="IMGDESC"><?php echo "No description"; ?></td>
                  <?php else: ?>
                    <td class="IMGDESC"><?php echo $img->picGallery_desc; ?></td>
                  <?php endif; ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
        </table>
      </div>
    </div>
  </div>

  <?php

      $this->load->view('templates/copyright');
      // this is the copyright date.
    ?>
