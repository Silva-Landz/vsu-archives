
    <div class="container">
        <div class="row">
            <h3 class="text-center"><b>You don't have access in this page.</b></h3>
            <p class="text-center">Click <a href="<?php echo base_url('login/index'); ?>">here</a> to login</p>
        </div>
    </div>
