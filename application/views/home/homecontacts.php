<div id="wrapper">
  
  <?php $this->load->view('templates/sidenav'); ?>

  <div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">

          <ol class="breadcrumb">
            <li>You're now here: </li>
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Contacts</li>
          </ol>

          <div class="text-center">

            <h3>Contact Us</h3>

            <p>Our friendly employee can be reached Monday through Friday, from 8:00am to 5pm at VSU Administration Building, ViSCA, Baybay City, Leyte.
            For complaints, please call Dr. Lourdes B. Cano (Tel. No. 563-7643) or Dr. Remberto A. Patindol (Tel. No. 563-7108) or you may drop your concerns
            in the suggestion box placed at the Administration Building.</p><br>

            <iframe class="embed-responsive-item z-depth-2" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.8360819980553!2d124.79439781435015!3d10.747113542341431!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33079715643e53e7%3A0x7f6d7da24f1b013c!2sVSU+Administration+Building%2C+Baybay+City%2C+Leyte!5e0!3m2!1sen!2sph!4v1483586468495" width="650" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          
          </div><br>

          <div class="col-md-6 col-md-offset-3 text-center">
            <div class="row text-xs-center">
              <div class="col-md-4">
                <a class="btn-floating btn-small waves-effect waves-light blue"><i class="fa fa-map-marker"></i></a>
                <p>ViSCA, Baybay City, Leyte</p>
                <p>Philippines</p>
              </div>
              <div class="col-md-4">
                <a class="btn-floating btn-small waves-effect waves-light blue"><i class="fa fa-phone"></i></a>
                <p>563-7643 or 563-7108</p>
                <p>Mon - Fri, 8:00-4:00</p>
              </div>
              <div class="col-md-4">
                <a class="btn-floating btn-small waves-effect waves-light blue"><i class="fa fa-envelope"></i></a>
                <p>N/A</p>
                <p>N/A</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('templates/copyright'); ?>

  </div>
</div>
