<div id="wrapper">

  <?php $this->load->view('templates/sidenav'); ?>

  <div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row">
       <div class="col-md-12">
        <ol class="breadcrumb">
          <li>You're now here: </li>
          <li><a href="<?php echo base_url(); ?>">Home</a></li>
          <li class="active">Downloads</li>
        </ol>

        <div class="text-center">
          <h3>View and Download PDF files</h3>
          <p>Note: Disabled buttons means that the file is not yet available, still on process of loading it.</p>
          <hr>
        </div>

        <div>
          <!-- Return to Top -->
          <a href="#" id="return-to-top" style="z-index:1;"><i class="fa fa-chevron-up"></i></a>

          <div class="container-fluid">

            <div class="row col-sm-12 text-center">
              <form action="<?php echo base_url(). 'homepage/downloads'; ?>" method="post">
                <div class="form-group">
                  <input type="text" style="height: 2.0rem !important; margin: 0 0 5px 0 !important; width: 50% !important; " name="txtSearchRecord" placeholder="Search records">

                  <button type="submit" title="Click to Search" class="btn btn-primary btn-sm" style="border-radius: 50%;"><span class="glyphicon glyphicon-search" style="margin-bottom: 0px !important; margin-right: 0px !important;"></span></button>
                </div>
              </form>
            </div>

          </div>

          <hr style="border: transparent;">

            <div class="row">

              <div class="col-md-12">

                <table class="table table-hover table-responsive" id="spreadsheet">
                  <thead>
                    <tr style="background-color:#0B791C; color: white; border-bottom: solid 4px #fff;">
                      <th class="sort col-xs-1">ID.</th>
                      <th class="sort col-xs-8">RECORDS DESCRIPTION</th>
                      <th class="sort col-xs-3">CATEGORY</th>
                      <th colspan="2"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $counter = 0;
                    foreach ($table as $res): ?>
                      <tr class="">
                        <td class='#'><?php echo $res->records_id; ?></td>
                        <?php
                          if ($res->records_dateField == ""): ?>
                            <td class="RECORDS DESCRIPTION" id='desc'><?php echo $res->records_desc; ?></td>
                          <?php
                            else: ?>
                              <td class="RECORDS DESCRIPTION" id='desc'><?php echo $res->records_desc." (". $res->records_dateField .")"; ?></td>
                        <?php endif; ?>
                        <td class='CATEGORY'><?php echo $res->category_name; ?></td>
                        <td>
                          <?php
                          if ($res->fileSize == "" || $res->fileSize == 0 || $res->fileSize == null): ?>
                            <a class="btn btn-success btn-sm disabled" href="<?php echo base_url('home/viewPDF/'). $res->records_id; ?>">View PDF</a>
                            <?php
                          else: ?>
                            <a class="btn btn-success btn-sm" target="_blank" href="<?php echo base_url('home/viewPDF/'). $res->records_id; ?>">View PDF</a>
                          <?php endif; ?>
                        </td>
                        <td>
                          <?php
                          if ($res->fileSize == "" || $res->fileSize == 0 || $res->fileSize == null): ?>
                            <a class="btn btn-primary btn-sm disabled" href="<?php echo base_url('home/download/'). $res->records_id; ?>">Download</a>
                            <?php
                          else: ?>
                            <a class="btn btn-primary btn-sm" href="<?php echo base_url('home/download/'). $res->records_id; ?>">Download</a>
                          <?php endif; ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                      <tr>
                        <td colspan="5" class="text-center no-result" style="display: none;"><p>No match data found</p></td>
                      </tr>
                  </tbody>

                </table>

              <div class="text-center"><span><?php echo $links; ?></span></div>

            </div>
          </div>

          </div>
       </div>
      </div>
    </div>

    <?php $this->load->view('templates/copyright'); ?>

  </div>
</div>
