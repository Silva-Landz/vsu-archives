<div id="wrapper">

  <?php $this->load->view('templates/sidenav'); ?>

  <div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row-fluid">
        <ol class="breadcrumb">
          <li>You're now here: </li>
          <li class="active">Home</li>
        </ol>
        <div class="text-center">
          <h3>Overview</h3>
          <hr>
        </div>
        <div class="col-sm-6 col-md-6">
          <h4>Electronic Records Management System (ERMS)</h4>
          <p class="text-muted">ERMS is a computer program or set of programs designed to track and store records. The software can be used to manage the creation and maintenance of records within classification schemes, apply retention and disposal schedules, and control access and use.</p>
        </div>
        <div class="col-sm-6 col-md-6 text-center">
          <img class="img img-responsive" src="<?php echo base_url(). 'img/erms.jpg'; ?>" alt="Electronic-Records-Management">
        </div>
      </div>
    </div><br><br>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="col-sm-6 col-md-6 text-center">
          <img class="img img-responsive" src="<?php echo base_url(). 'img/2.png'?>" alt="" width="400" height="337">
        </div>
        <div class="col-sm-6 col-md-6">
          <h4>Records Management</h4>
          <p class="text-muted">We offer a full line of records management services geared toward improving day-to-day office efficiency and freeing up valuable workspace. We store our client’s records in a professionally outfitted records center at literally cents per box. Files can be retrieved as quickly as possible.</p>
        </div>
      </div>
    </div><br>

    <div class="container-fluid">
      <div class="row-fluid">

        <?php $this->load->view('templates/citizenscharter'); ?>

      </div>
    </div>

    <?php $this->load->view('templates/copyright'); ?>

  </div>
</div>
