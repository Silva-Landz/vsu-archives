<div id="wrapper">

  <?php $this->load->view('templates/sidenav'); ?>

  <div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">

          <ol class="breadcrumb">
            <li>You're now here: </li>
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">About</li>
          </ol>

         <section class="text-center">
           <!--Section heading-->
           <h3>Our amazing team</h3>
           <!--Section description-->
           <p><b>Vision: </b>Center of exellence on education and research in agriculture and allied fields in the Visayas.</p>
           <p><b>Mission: </b>Attainment of the highest quality of human capital and scientific knowledge for the sustained growth and development of agriculture, fisheries, forestry and agro-industries.</p>
           <hr>
         </section>

        </div>
      </div>

      <div class="row text-center">
         <div class="col-sm-12">
            <?php foreach ($staffs as $staff): ?>
               <div class="col-lg-4">
                 <div class="avatar">
                   <img src="<?php echo base_url('img/faculties/'). $staff->filename; ?>" class="img img-circle z-depth-2" height="150" width="150">
                 </div>
                 <h5><?php echo $staff->firstname. " ". $staff->lastname; ?></h5>
                 <p class="text-muted"><?php echo $staff->pos_type; ?></p>
               </div>
            <?php endforeach; ?>
         </div>
      </div>

    </div>

    <?php $this->load->view('templates/copyright'); ?>

  </div>
