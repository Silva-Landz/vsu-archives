<div id="wrapper">
  
  <?php $this->load->view('templates/sidenav'); ?>

  <div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li>You're now here:</li>
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Gallery</li>
          </ol>

          <h3 class="text-center">Explore the Gallery</h3>
          <p class="text-center">Note: You can download the image by pressing right click on the image then click "Save image as".</p><hr>

          <!-- image gallery here -->
          <div class="container" style="padding-left:20px;width:auto;">
            <div class="thumbnails">

              <?php foreach($images as $img): ?>

                <a href="<?php echo base_url('img/archives/'). $img->filename; ?>" data-lightbox="<?php echo $img->tags; ?>" data-title="<?php echo $img->picGallery_title. '<br>' .$img->picGallery_desc; ?>">
                  <img class="z-depth-1 img img-responsive img-thumbnail" src="<?php echo base_url('img/archives/'). $img->filename; ?>" alt="<?php echo $img->filename; ?>" width="150" height="150">
                </a>

              <?php endforeach; ?>
                
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('templates/copyright'); ?>

  </div>
</div>
