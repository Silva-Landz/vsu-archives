
<?php $this->load->view('templates/topnav'); ?>

<div id="table" class="container">
  <!-- Return to Top -->
  <a href="#" id="return-to-top" style="z-index:1; "><i class="fa fa-chevron-up"></i></a>
  <!-- Search engine starts here! -->
  <div class="row">
    <h3><b>Records and Archives</b></h3>
    <div class="col-md-6 col-md-offset-3">
      <div class="input-field">
        <input type="text" class="search" placeholder="Search Records" size="30"/>
      </div>
    </div>
  </div>
  <!-- Search engine ends here! -->
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover">
        <thead style="background-color:#25ACD6;color:#FFFFFF">
          <th class="sort col-xs-2" data-sort="#" id="num">NO.</th>
          <th class="sort col-xs-7" data-sort="RECORDS DESCRIPTION">RECORDS DESCRIPTION</th>
          <th class="sort col-xs-3" data-sort="CATEGORY">CATEGORY</th>
          <th class="text-center" colspan="3">ACTIONS</th>
        </thead>
        <tbody class="list">
          <?php
          $counter = 0;
          foreach ($table as $res): ?>
          <tr>
            <td class="#"><?php echo ++$counter; ?></td>
            <?php if ($res->records_dateField == ""): ?>
              <td class='RECORDS DESCRIPTION' id='desc'><?php echo $res->records_desc; ?></td>
            <?php else: ?>
              <td class='RECORDS DESCRIPTION' id='desc'><?php echo $res->records_desc ." (".$res->records_dateField.")"; ?></td>
            <?php endif; ?>
            <td class='CATEGORY'><?php echo $res->category_name; ?></td>
            <td> <?php echo "<a type='button' class='btn btn-default btn-sm' href='".base_url('home/view')."/$res->records_id' title='View record'><i class='material-icons'>&#xE8F4;</i></a>"; ?></td>
            <td> <?php echo "<a class='btn btn-primary btn-sm' href='".base_url('home/edit')."/$res->records_id' title='Edit record'><i class='material-icons'>&#xE150;</i></a>"; ?></td>
            <td> <?php echo "<a class='btn btn-danger btn-sm' href='".base_url('home/delete')."/$res->records_id' onclick='return deleteConfirm();' title='Delete record'><i class='material-icons'>&#xE872;</i></a>"; ?></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
        <div class="row">
          <div class="col-md-12" >
            <?php echo "<p style='float:left;padding-top:25px;'>found results: ". $counter ."</p>"; ?>
            <a id="addNew" href="<?php echo base_url('home/add');?>" title='Add new record' class='btn-floating btn-small waves-effect waves-light blue pull-right'><i class="material-icons">&#xE145;</i></a>
          </div>
        </div>
      </table>
    </div>
  </div>
</div>
<hr>
<div class="container-fluid" style="margin:50px 0px;">
  <div class="row text-center">
    <div class="footer">
      Copyright 2017 Records Office and Archives Center <br>
      <span class="text-muted">version 1.0</span>
    </div>
  </div>
</div>
