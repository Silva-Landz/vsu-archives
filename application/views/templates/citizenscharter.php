
<h3 class="text-center">Citizen's Charter</h3>
<hr>
<h5><i class="fa fa-clipboard"></i> Request for copy of records</h5>
<p class="text-muted">The Records Office and Archives Center would like to express its willingness to extend and render
efficient records services to its clients. Records requested that are found in files will be released
immediately upon receipt of the duly subscribed request form. However, request for documents of personnel
other than employee concerned and other confidential records shall be released only if with written approval
from the University President or authorized university officials.</p>

<ol type="I">
  <li><p>Required Documents</p></li>
  <ul>
    <li>
      <p class="text-muted">- Approved letter request/subscribed request form</p>
    </li>
  </ul>
  <li><p>Schedule of Availability of Service</p></li>
  <ul>
    <li>
      <p class="text-muted">- Monday-Friday: 8:00 A.M. - 5:00 P.M.</p>
    </li>
  </ul>
  <li><p>Steps to Avail Services</p></li>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th class="col-sm-1 text-center">Step</th>
        <th class="col-sm-3 text-center">Applicant/Client</th>
        <th class="col-sm-3 text-center">Office Activity</th>
        <th class="col-sm-2 text-center">Duration</th>
        <th class="col-sm-3 text-center">Person in charge</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="text-center">1</td>
        <td>Fills up/signs a request formlisting down the needed documents</td>
        <td>Provides the request form and have the form subscribed by the legal officer/approved by the President</td>
        <td class="text-center">10 minutes</td>
        <td class="text-center">Ms. Graciana M. Espinosa / Mr. Vergillo C. Acilo</td>
      </tr>
      <tr>
        <td class="text-center">2</td>
        <td>Proceeds to the Legal Office/Office of the President</td>
        <td></td>
        <td class="text-center"></td>
        <td></td>
      </tr>
      <tr>
        <td class="text-center">3</td>
        <td>Submits the subscribed/approved form to the staff in charge</td>
        <td>Retrieves the requested documents and photocopies the documents</td>
        <td class="text-center">30 minutes</td>
        <td class="text-center">Ms. Graciana M. Espinosa / Mr. Vergillo C. Acilo</td>
      </tr>
      <tr>
        <td class="text-center">4</td>
        <td>Receives/reviews the documents</td>
        <td>Certifies the request documents, if necessary and approves its release</td>
        <td class="text-center">30 minutes</td>
        <td class="text-center">Ms. Graciana M. Espinosa / Mr. Vergillo C. Acilo / Ms. Asteria A. Sevilla</td>
      </tr>
    </tbody>
  </table>
  Contacts: <a href="<?php echo base_url('homepage/contacts'); ?>">Click here!</a>
</ol>
<div class="text-right" style="padding-right:40px;">
  <p><b>ASTERIA A. SEVILLA</b><br><span style="padding-right:13px;">Officer-in-Charge</span></p>
</div>
