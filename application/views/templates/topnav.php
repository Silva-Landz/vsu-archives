<!--Navbar-->
<nav class="navbar navbar-dark bg-primary">
  <!-- Collapse button-->
  <button class="navbar-toggler hidden-sm-up btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseEx2" style="background:transparent;border:0px;margin-top:8px;">
  <i class="material-icons">&#xE5D2;</i>
  </button>
  <a href="<?php echo base_url('login/logout'); ?>" class="btn-sm btn-primary pull-right" style="margin-top:10px;margin-right:10px;background:transparent" title="logout"><span class="fa fa-sign-out"></span>Logout</a>
  <div class="container">
    <!--Collapse content-->
    <div class="collapse navbar-toggleable-xs" id="collapseEx2">
      <!--Links-->
      <ul class="nav navbar-nav">
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>"><b>Records Office</b></a></li>
        <li class="nav-item active"><a class="nav-link">Archives <span class="sr-only">(current)</span></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url('image/image_gallery'); ?>">Gallery</a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url('about/faculties'); ?>">About</a></li>
      </ul>
      <!-- End Links -->
    </div>
    <!--/.Collapse content-->
  </div>
  <!-- End container -->
</nav>
<!--/.Navbar-->