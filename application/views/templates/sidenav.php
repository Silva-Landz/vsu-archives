<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
      <li class="sidebar-brand">
        <a href="<?php echo base_url(); ?>" style="margin:10px 0px"><img src="<?php echo base_url(). 'img/favicon-logo.ico'?>" width="55" height="55" class="center-block img-responsive" alt=""></a>
      </li>
      <li>
        <a href="<?php echo base_url('homepage/gallery'); ?>"><span class="glyphicon glyphicon-picture"></span>Gallery</a>
      </li>
      <li>
        <a href="<?php echo base_url('homepage/downloads'); ?>"><span class="glyphicon glyphicon-download"></span>Downloads</a>
      </li>
      <li class="active">
        <a href="<?php echo base_url('homepage/about'); ?>"><span class="glyphicon glyphicon-user"></span>About</a>
      </li>
      <li>
        <a href="<?php echo base_url('homepage/contacts'); ?>"><span class="glyphicon glyphicon-phone"></span>Contacts</a>
      </li>
      <li>
        <a href="<?php echo base_url('login/index'); ?>"><span class="glyphicon glyphicon-log-in"></span>Login</a>
      </li>
    </ul>
  </div>
  
  <a href="#menu-toggle" class="btn btn-primary btn-sm" id="menu-toggle" style="position:fixed;z-index:1;"><i class="material-icons">&#xE5D2;</i></a>

  <div class="container-fluid" style="border-bottom:solid 1px lightgray;padding-top:15px;padding-bottom:20px;">
    <img src="<?php echo base_url(). 'img/vsu-logo.png'; ?>" class="center-block img-responsive" alt="vsu-archives-logo"></a>
  </div>