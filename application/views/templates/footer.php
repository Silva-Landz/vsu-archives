
    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url(). 'assets/js/jquery.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url(). 'assets/jqueryui/jquery-ui.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url(). 'assets/jqueryui/jquery-ui.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url(). 'assets/js/jquery.jscroll.min.js'; ?>"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(). 'assets/js/bootstrap.min.js'; ?>"></script>

    <!-- Material Design Bootstrap -->
    <script type="text/javascript" src="<?php echo base_url(). 'assets/js/mdb.js'; ?>"></script>

    <!--  List.js here! -->
    <script type="text/javascript" src="<?php echo base_url(). 'assets/js/list.js'; ?>"></script>

    <!-- lightbox -->
    <script type="text/javascript" src="<?php echo base_url(). 'assets/js/lightbox.js'?>"></script>
    <!-- SCRIPTS -->

    <!-- start search plugin -->
    <script type="text/javascript">
        var options = {
            valueNames: ['#', 'RECORDS DESCRIPTION', 'CATEGORY', 'SHELF NO.', 'SHELF POSITION', 'IMGTITLE', 'IMGDESC']
        };
        
        var recordsInfo = new List('table', options);
        
        $('.search').on('keyup', function(event) {
          if ($('.list tr').length === 0) {
            $('.no-result').css('display', 'block');
          }
          else {
            $('.no-result').css('display', 'none');
          }
        });


        $('document').ready(function()  {
          $('#datePicker').datepicker({
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            monthNames: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]
          });
        });

    </script>
    <!--  end search plugin -->

    <script type="text/javascript">

        $(window).scroll(function() {
          if ($(this).scrollTop() >= 50) {      // If page is scrolled more than 50px
            $('#return-to-top').fadeIn(200);    // Fade in the arrow
          }
          else {
            $('#return-to-top').fadeOut(200);   // Else fade out the arrow
          }
        });

        $('#return-to-top').click(function() {   // When arrow is clicked
          $('body,html').animate({
            scrollTop : 0                      // Scroll to top of body
          }, 500);
        });



        function deleteConfirm()  {
          if (confirm("Are you sure you want to delete this?")) {
            return true;
          }
          else {
            return false;
          }
        }

        $('document').ready(function()  {
          
          /* toggle menu */
          $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
          });

          /* jScroll */
          $('.scroll').jscroll();

        });

    </script>
    

  </body>
</html>
