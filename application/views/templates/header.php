<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title><?php echo $title; ?></title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(). 'assets/css/font-awesome.min.css'; ?>">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url(). 'assets/bootstrap/bootstrap_css/bootstrap.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo base_url(). 'assets/bootstrap/bootstrap_css/bootstrap.css'; ?>">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(). 'assets/css/bootstrap.min.css'; ?>" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(). 'assets/css/mdb.css'; ?>" rel="stylesheet">

    <link href="<?php echo base_url(). 'assets/css/style.css'; ?>" rel="stylesheet">
    <link href="<?php echo base_url(). 'assets/css/simple-sidebar.css'; ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(). 'assets/css/lightbox.css'?>">

    <link rel="stylesheet" href="<?php echo base_url(). 'assets/jqueryui/jquery-ui.css'; ?>">
    <link rel="stylesheet" href="<?php echo base_url(). 'assets/jqueryui/jquery-ui.min.css'; ?>">

    <link rel="shortcut icon" href="<?php echo base_url(). 'favicon-logo.ico'; ?>" type="image/x-icon">
  </head>
<body>
