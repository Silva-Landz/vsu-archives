<!--Navbar-->
<nav class="navbar navbar-dark bg-primary">
  <button class="navbar-toggler hidden-sm-up btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseEx2" style="background:transparent;border:0px;margin-top:8px;">
    <i class="material-icons">&#xE5D2;</i>
  </button>

  <a href="<?php echo base_url('login/logout'); ?>" class="btn-sm btn-primary pull-right" style="margin-top:10px;margin-right:10px;background:transparent" title="logout"><span class="fa fa-sign-out"></span>Logout</a>

  <div class="container">
    <div class="collapse navbar-toggleable-xs" id="collapseEx2">
      <ul class="nav navbar-nav">
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>"><b>Records Office</b></a></li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('home/index'); ?>">Archives</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('image/image_gallery'); ?>">Gallery</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo base_url('about/getStaff'); ?>">About <span class="sr-only">(current)</span></a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
  <div class="row">
    <h3><b>Faculty</b></h3>
    <hr>
    <div class="col-sm-6" style="border:solid 3px #ccc;padding:20px 30px;">
      <h4>Add Faculty</h4><span class="text-muted">Note: The following field that has * is required to fill in.</span>
      <hr>

      <form action="<?php echo base_url('about/addEmployee'); ?>" method="post" enctype="multipart/form-data">
        <div class="row">
           <div class="col-md-6">
                <input type="text" class="form-control" id="" name="txtFname" placeholder="Firstname *" required>
           </div>
           <div class="col-md-6">
                <input type="text" class="form-control" id="" name="txtLname" placeholder="Lastname *" required>
           </div>
        </div>
        <div class="row">
           <div class="col-md-6">
                <input type="text" class="form-control" id="" name="txtMname" placeholder="Middlename *" required>
           </div>
           <div class="col-md-6">
                <input type="text" class="form-control" id="datePicker" name="txtBday" placeholder="Birthdate *">
           </div>
        </div>
        <div class="row">
           <div class="col-md-12">
              <input type="text" class="form-control" id="" name="txtAddress" placeholder="Current Address">
           </div>
        </div>
        <div class="row">
           <div class="col-md-6">
              <label for="">Position *</label>
              <select class="form-control" name="slctPosition" required>
                <option value="">Choose...</option>
                <?php foreach ($position as $pos): ?>
                  <option value="<?php echo $pos->pos_id; ?>"><?php echo $pos->pos_type; ?></option>
                <?php endforeach; ?>
              </select>
           </div>
           <div class="col-md-6">
              <label>Upload photo</label>
              <input type="file" class="" name="fileProfImg" accept="image/*">
           </div>
        </div>
        <br>
        <div class="row">
           <div class="form-group text-right">
             <button type="submit" class="btn btn-primary">Save changes</button>
           </div>
        </div>
      </form>
    </div>

    <div class="col-sm-6 col-md-6">
        <?php foreach ($staffs as $s): ?>
          <div class="well well-sm" style="border-radius:0px;">
              <div class="row">
                  <div class="col-sm-3 col-md-4">
                    <?php if ($s->filename == null || $s->filename == ""): ?>
                      <img src="<?php echo base_url('img/faculties/'). 'noimage.jpg'; ?>" width="160" height="180" alt="noimage">
                      <?php else: ?>
                        <img src="<?php echo base_url('img/faculties/'). $s->filename; ?>" width="380" height="500" alt="" class="img-responsive" />
                    <?php endif; ?>
                  </div>
                  <div class="col-sm-6 col-md-8">
                    <h5><?php echo $s->lastname .", ". $s->firstname; ?></h5>
                    <small><cite title="<?php echo $s->pos_type; ?>"><?php echo $s->pos_type; ?></cite></small>
                    <p>
                      <i class="glyphicon glyphicon-map-marker"></i><?php echo $s->address; ?>
                      <br />
                      <i class="glyphicon glyphicon-gift"></i><?php echo $s->bday; ?>
                    </p>
                  </div>
                  <div class="col-md-8 col-sm-offset-4">
                    <a href="<?php echo base_url('about/getSelectedStaff/'). $s->staff_id;?>" class="btn btn-info">Edit</a><span>
                      <a href="<?php echo base_url('about/deleteSelectedStaff/'). $s->staff_id; ?>" class="btn btn-danger" onclick="return deleteConfirm();">Delete</a></span>
                  </div>
              </div>
          </div>
        <?php endforeach; ?>
    </div>
  </div>
</div>
