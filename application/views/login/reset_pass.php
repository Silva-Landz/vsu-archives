
<div class="container">
  <div class="row">
    <div class="col-sm-7">
      <h3>Forgot Password</h3>
      <p>Enter your email address, and we'll send you a passowrd reset email.</p>

      <form class="" accept-charset="utf-8" action="#" method="post">
        <div class="form-group text-center">
          <div class="col-sm-8">
            <input type="email" name="resetemail" placeholder="example@email.com">
          </div>
          <div class="form-group">
            <button type="submit" name="btnreset" class="btn btn-primary" style="background:#138D75;" disabled>Reset Password</button>
          </div>
        </div>
      </form>

      <p class="alert-warning" style="display:inline-block;padding-left:6px;"><strong>Note:</strong> The resetting of password is not yet available. Please try to contact your administrator to recover your password.</p>
      <p><a href="<?php echo base_url(); ?>">Click here to go back to the homepage</a></p>
    </div>
  </div>
</div>
