<div class="container">
  <div class="row">
    <div class="col-sm-12 text-center" style="margin-bottom:30px;margin-top:auto; ">
      <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(). 'img/vsu-logo.png'; ?>" class="center-block img-responsive" width="500" alt="vsu-archives-logo"></a>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <p class="text-center text-danger"><b>Account Registration</b></p>
    <div class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4" style="border:solid 1px #ccc;padding:20px 30px;">
      <form class="form-signin" action="<?php echo base_url('login/register'); ?>" method="post">
        <div class="form-group pull-center">
          <img src="https://image.flaticon.com/icons/svg/270/270013.svg" height="100" width="100" class="center-block img-responsive">
        </div>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="text" id="inputEmail" class="form-control" name="txtEmail" placeholder="Username">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" name="txtPass" placeholder="Password">
        <label for="confirmPass" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" name="txtConfirmPass" placeholder="Confirm Password">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
      </form>
      <div class="text-center">
        <?php echo validation_errors('<span class="text-danger"><b>', '</b></span>'); ?>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('templates/copyright'); ?>