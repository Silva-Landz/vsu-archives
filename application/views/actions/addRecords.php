<!--Navbar-->
<nav class="navbar navbar-dark bg-primary">
  <!-- Collapse button-->
  <button class="navbar-toggler hidden-sm-up btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseEx2" style="background:transparent;border:0px;margin-top:8px;">
    <i class="material-icons">&#xE5D2;</i>
  </button>

  <a href="<?php echo base_url('login/logout'); ?>" class="btn-sm btn-primary pull-right" style="margin-top:10px;margin-right:10px;background:transparent" title="logout">Logout</a>

  <div class="container">
    <!--Collapse content-->
    <div class="collapse navbar-toggleable-xs" id="collapseEx2">
        <!--Links-->
        <ul class="nav navbar-nav">
          <li class="nav-item"><a class="nav-link"><b>Records Office</b></a></li>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url('home/index');?>">Archives <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('image/image_gallery'); ?>">Gallery</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('about/faculties'); ?>">About</a>
          </li>
        </ul>
        <!-- End Links -->
    </div>
    <!--/.Collapse content-->
  </div>
</nav>
<!--/.Navbar-->

<!--  Form Adding new record Start here -->
<div class="container" id="form-container">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <h4>Add Record</h4>
      <hr>
      <?php echo form_open_multipart('home/save');?>
        <div class="form-group">
          <label for="exampleInputEmail1">DESCRIPTION (required)</label>
          <textarea name="txtDesc" class="form-control" cols="20" rows="3" placeholder="Enter the description" required></textarea>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">DATEFIELD</label>
          <textarea name="txtDate" class="form-control" cols="20" rows="3" placeholder="Enter the corresponding dates"></textarea>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="exampleDropDownCategory">RECORDS CATEGORY (required)</label>
              <select name="slctCat" class="form-control" required>
                <option value="">Choose...</option>
                <?php foreach ($category as $cat): ?>
                  <option value="<?php echo $cat->category_id; ?>"><?php echo $cat->category_name; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="exampleDropDownCategory">RECORDS LOCATION (required)</label>
              <select name="slctShelfNumAndPos" class="form-control" required>
                <option value="">Choose...</option>
                <?php foreach ($shelve as $shelf): ?>
                  <option value="<?php echo $shelf->shelves_id; ?>">Shelf <?php echo $shelf->shelves_num ." - ". $shelf->shelves_pos; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-10">
            <div class="form-group">
              <label for="exampleInputFile" style="padding-top:10px">FILE INPUT (required)</label>
              <input type="file" class="form-control" name="uploadFile" accept=".pdf" required>
            </div>
          </div>
        <div>
        <div class="form-group">
          <label for="exampleRecID">PAGE(S)</label>
          <div class="row">
            <div class="col-md-2">
              <input type="text" class="form-control" name="numPages" value="0">
            </div>
          </div>
        </div>
        
        <button type="submit" name="button" class="btn btn-primary">Save</button>

        <?php echo form_close();?>
        <!-- this closes the form -->
    </div>
  </div>
</div>

<?php

  $this->load->view('templates/copyright');

  ?>
