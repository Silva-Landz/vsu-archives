  <!--Navbar-->
  <nav class="navbar navbar-dark bg-primary">
    <!-- Collapse button-->
    <button class="navbar-toggler hidden-sm-up btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseEx2" style="background:transparent;border:0px;margin-top:8px;">
      <i class="material-icons">&#xE5D2;</i>
    </button>

    <a href="<?php echo base_url('login/logout'); ?>" class="btn-sm btn-primary pull-right" style="margin-top:10px;margin-right:10px;background:transparent" title="logout">Logout</a>

    <div class="container">
      <!--Collapse content-->
      <div class="collapse navbar-toggleable-xs" id="collapseEx2">
        <!--Links-->
        <ul class="nav navbar-nav">
          <li class="nav-item"><a class="nav-link"><b>Records Office</b></a></li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('home/index'); ?>">Archives</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('image/image_gallery'); ?>">Gallery</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('about/faculties'); ?>">About <span class="sr-only">(current)</span></a>
          </li>
        </ul>
        <!-- End Links -->
      </div>
      <!--/.Collapse content-->
    </div>
  </nav>
  <!--/.Navbar-->

  <div class="container">
      <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
          <h4>Edit Image</h4><hr>

          <?php foreach ($images as $img): ?>

            <form action="<?php echo base_url('image/updateImage/'). $img->picGallery_id; ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="txtImgId" value="<?php echo $img->picGallery_id; ?>">
            <div class="form-group">
              <label for="">Title</label>
              <input type="text" name="txtImgTitle" value="<?php echo $img->picGallery_title; ?>" placeholder='Enter title'>
            </div>
            <div class="form-group">
              <label for="">Description</label>
              <textarea name="txtImgDescription" class="form-control" rows="5" cols="80" style="border-radius:0";><?php echo $img->picGallery_desc; ?></textarea>
            </div>
            <div class="form-group">
              <label for="">Tags</label>
              <select class="form-control" name="slctImgTags" required>
                <option value="">Choose...</option>
                <?php foreach ($imgTags as $tags): ?>
                  <?php if ($tags->tags_id == $img->tags_id): ?>
                    <option value="<?php echo $img->tags_id; ?>" selected="selected"><?php echo $img->tags; ?></option>
                    <?php else: ?>
                      <option value="<?php echo $tags->tags_id; ?>"><?php echo $tags->tags; ?></option>
                  <?php endif; ?>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="form-group">
              <label>Current Image</label><br>
              <div class="z-depth-2">
                <img src="<?php echo base_url(). 'img/archives/'. $img->filename; ?>" height="100%" width="100%">
              </div>
            </div>
            <div class="form-group">
              <label for="">Update Image</label>
              <input type="file" class="form-control" name="editImgFile" accept="image/*">
            </div><hr>

            <button type="submit" class="btn btn-primary pull-right" name="button">Save</button>
          
          </form>
          
          <?php endforeach; ?>
        
        </div>
      </div>
    </div>

  <?php $this->load->view('templates/copyright'); ?>
