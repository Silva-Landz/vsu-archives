  <nav class="navbar navbar-dark bg-primary">
    <button class="navbar-toggler hidden-sm-up btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseEx2" style="background:transparent;border:0px;margin-top:8px;">
      <i class="material-icons">&#xE5D2;</i>
    </button>

    <a href="<?php echo base_url('login/logout'); ?>" class="btn-sm btn-primary pull-right" style="margin-top:10px;margin-right:10px;background:transparent" title="logout">Logout</a>

    <div class="container">
      <div class="collapse navbar-toggleable-xs" id="collapseEx2">
        <ul class="nav navbar-nav">
          <li class="nav-item"><a class="nav-link"><b>Records Office</b></a></li>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url('home/index'); ?>">Archives <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('image/image_gallery'); ?>">Gallery</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('about/faculties'); ?>">About</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!--  Form Edit Start here -->
  <div class="container" id="form-container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <h4>Edit Record</h4><hr>
        <?php foreach ($records as $rec): ?>
          
          <?php echo form_open_multipart('home/update/'. $rec->records_id);?>
          
            <input type="hidden" name="recID" class="form-control" value="<?php echo $rec->records_id; ?>">
            <div class="form-group">
              <label for="exampleInputEmail1">Description</label>
              <textarea name="txtDesc" class="form-control" cols="20" rows="3" placeholder="Enter the description" required><?php echo $rec->records_desc; ?></textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Datefield</label>
              <textarea name="txtDate" class="form-control" cols="20" rows="3" placeholder="Enter the corresponding dates"><?php echo $rec->records_dateField; ?></textarea>
            </div>
            <div class="row">
              <div class="col-md-7">
                <div class="form-group">
                    <label for="exampleDropDownCategory">Category</label>
                    <select name="slctCat" class="form-control" required>
                      <option value="">Choose...</option>
                        <?php foreach ($category as $cat): ?>
                          <?php if ($cat->category_id == $rec->category_id): ?>
                            <option value="<?php echo $rec->category_id; ?>" selected="selected"><?php echo $rec->category_name; ?></option>
                            <?php else: ?>
                              <option value="<?php echo $cat->category_id; ?>"><?php echo $cat->category_name; ?></option>
                          <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
              </div>
              <div class="col-md-5">
              <label for="exampleDropDownCategory">Location</label>
                <select name="slctShelfNumAndPosEdit" class="form-control" required>
                  <option value="">Choose...</option>
                  <?php foreach ($shelve as $shelf): ?>
                    <?php if ($rec->shelves_id == $shelf->shelves_id): ?>
                      <option value="<?php echo $rec->shelves_id; ?>" selected="selected">Shelf <?php echo $shelf->shelves_num ." - ". $shelf->shelves_pos; ?></option>
                      <?php else: ?>
                        <option value="<?php echo $shelf->shelves_id; ?>">Shelf <?php echo $shelf->shelves_num ." - ". $shelf->shelves_pos; ?></option>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-12">
                  <label for="exampleInputFile" style="padding-top:10px">Update file</label>
                  <input type="file" class="form-control" name="editUploadFile" accept=".pdf">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-10">
                <div class="form-group">
                  <label for="exampleInputFile">Filename</label>
                  <p style="padding-top: 12px">
                    <?php
                      if($rec->fileName == '0' || $rec->fileName == "") {
                        echo "No file available. Please insert a file. ";
                      }
                      else {
                        echo $rec->fileName;
                      }
                    ?>
                  </p>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleRecID">PAGE(S)</label>
                  <input type="text" class="form-control" name="numPages" value="<?php echo $rec->pages; ?>">
                </div>
              </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right">Save changes</button>

          <?php echo form_close();?>

        <?php endforeach; ?>

      </div>
    </div>
  </div>
  <!--  Form Edit End here -->
  <?php $this->load->view('templates/copyright'); ?>
