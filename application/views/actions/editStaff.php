
    <!--Navbar-->
    <nav class="navbar navbar-dark bg-primary">

      <!-- Collapse button-->
      <button class="navbar-toggler hidden-sm-up btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseEx2" style="background:transparent;border:0px;margin-top:8px;">
        <i class="material-icons">&#xE5D2;</i>
      </button>

      <a href="<?php echo base_url('login/logout'); ?>" class="btn-sm btn-primary pull-right" style="margin-top:10px;margin-right:10px;background:transparent" title="logout">Logout</a>

      <div class="container">

          <!--Collapse content-->
          <div class="collapse navbar-toggleable-xs" id="collapseEx2">
              <!--Links-->
              <ul class="nav navbar-nav">
                <li class="nav-item"><a class="nav-link"><b>Records Office</b></a></li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url('home/index'); ?>">Archives</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url('image/image_gallery'); ?>">Gallery</a>
                </li>
                <li class="nav-item active">
                  <a class="nav-link" href="<?php echo base_url('about/faculties'); ?>">About <span class="sr-only">(current)</span></a>
                </li>
              </ul>
              <!-- End Links -->
          </div>
          <!--/.Collapse content-->
      </div>

    </nav>
    <!--/.Navbar-->

    <div class="container">
      <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <h4>Edit Faculty</h4>
            <hr>

            <?php foreach ($selectedStaffs as $ss): ?>

              <form action="<?php echo base_url('about/updateEmployee/'). $ss->staff_id; ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="txtStaffId" value="<?php echo $ss->staff_id; ?>">
                <div class="form-group">
                  <label for=""><span class="glyphicon glyphicon-user"></span>Firstname</label>
                  <input type="text" class="form-control" id="" name="txtFirstname" value="<?php echo $ss->firstname; ?>" placeholder="Enter firstname">
                </div>
                <div class="form-group">
                  <label for=""><span class="glyphicon glyphicon-user"></span>Lastname</label>
                  <input type="text" class="form-control" id="" name="txtLastname" value="<?php echo $ss->lastname; ?>" placeholder="Enter lastname">
                </div>
                <div class="form-group">
                  <label for=""><span class="glyphicon glyphicon-user"></span>Middlename</label>
                  <input type="text" class="form-control" id="" name="txtMiddlename" value="<?php echo $ss->middlename; ?>" placeholder="Enter middlename">
                </div>
                <div class="form-group">
                  <label for=""><span class="glyphicon glyphicon-gift"></span>Birthdate</label>
                  <input type="text" class="form-control" id="datePicker" name="txtBday" value="<?php echo $ss->bday; ?>" placeholder="Enter birthdate">
                </div>
                <div class="form-group">
                  <label for=""><span class="glyphicon glyphicon-map-marker"></span>Address</label>
                  <input type="text" class="form-control" id="" name="txtAddress" value="<?php echo $ss->address; ?>" placeholder="Enter address">
                </div>
                <div class="form-group">
                  <label for=""><span class="glyphicon glyphicon-list-alt"></span>Position</label>
                  <select class="form-control" name="slctPosition">
                    <option value="">Choose...</option>
                    <?php foreach ($position as $pos): ?>
                      <?php if ($ss->pos_id == $pos->pos_id): ?>
                        <option value="<?php echo $ss->pos_id; ?>" selected="selected"><?php echo $ss->pos_type; ?></option>
                      <?php else: ?>
                        <option value="<?php echo $pos->pos_id; ?>"><?php echo $pos->pos_type; ?></option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label><span class="glyphicon glyphicon-picture"></span>Current Image</label><br>
                  <img src="<?php echo base_url() .'img/faculties/'. $ss->filename; ?>" alt="<?php echo $ss->firstname; ?>" width="150" height="150" style="border:solid 1px #ccc">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile"><span class="glyphicon glyphicon-upload"></span>Update Image</label>
                  <input type="file" class="form-control" name="imgEditFile">
                </div><hr>

                <button type="submit" class="btn btn-primary pull-right">Save changes</button>
              </form>

            <?php endforeach; ?>

          </div>
      </div>
    </div>
    <?php $this->load->view('templates/copyright'); ?>
