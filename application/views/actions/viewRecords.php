
  <?php foreach ($viewRec as $vr): ?>

    <div class="container">
      <div class="row col-sm-12 text-center">
        <h4><b><?php echo $vr->records_desc; ?></b></h4>
      </div>
    </div>

    <hr>

    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-sm-offset-3 text-center" style="background-color: #eee; padding: 15px; border-top: 3px solid #AAACAD ">

          <p><b>ID: </b><?php echo $vr->records_id; ?></p>
          <p><b>DATEFIELD</b> <br>  
            <?php
              if ($vr->records_dateField == "") { echo "No date"; }
              else {  echo $vr->records_dateField;  }
            ?>
          </p>
          <p><b>CATEGORY</b> <br> <?php echo $vr->category_name; ?> </p>
          <p><b>LOCATION</b> <br> <?php echo $vr->shelves_num ." - ". $vr->shelves_pos; ?> </p>
          <p><b>FILENAME</b> <br> 
            <?php
              if($vr->fileName == '0' || $vr->fileName == "") { echo "No file available. Please insert a new file.";  }
              else { echo $vr->fileName; }
            ?>
          </p>
          <p><b>FILESIZE</b> <br> 
            <?php
              if($vr->fileSize >= 1000) { echo number_format($vr->fileSize, 1)." KB"; }
              if($vr->fileSize < 1000 && $vr->fileSize > 0) { echo number_format($vr->fileSize, 1)." KB"; }
              if($vr->fileSize == 0 || $vr->fileSize == "") {
                $vr->fileSize = 0;
                echo number_format($vr->fileSize)." KB";
              }
            ?>
          </p>
          <p><b>PAGE(S)</b> <br> <?php echo $vr->pages; ?></p>
      </div>
    </div>


    <div class="row">
      <div class="col-sm-6 col-sm-offset-3 text-center" style="background-color: #eee;">
        <div class="text-center">
          <?php if ($vr->fileSize == 0 || $vr->fileSize == ""): $vr->fileSize = 0;?>
            <a href="<?php echo base_url('home/download');?>/<?php echo $vr->records_id; ?>" class="btn btn-primary disabled" title="Download pdf disabled"><span class="glyphicon glyphicon-download"></span> Download (<?php echo number_format($vr->fileSize); ?> KB)</a>
            <span><a href="<?php echo base_url('home/viewPDF');?>/<?php echo $vr->records_id; ?>" target="_blank" class="btn btn-primary disabled" title="Read pdf file disabled"><span class="glyphicon glyphicon-book"></span> Read Online PDF</a></span>
          <?php endif; ?>

          <?php if ($vr->fileSize >= 1000): ?>
            <a href="<?php echo base_url('home/download');?>/<?php echo $vr->records_id; ?>" class="btn btn-primary" title="Download pdf"><span class="glyphicon glyphicon-download"></span> Download (<?php echo number_format($vr->fileSize); ?> KB)</a>
            <span><a href="<?php echo base_url('home/viewPDF');?>/<?php echo $vr->records_id; ?>" target="_blank" class="btn btn-primary" title="Read pdf file"><span class="glyphicon glyphicon-book"></span> Read Online PDF</a></span>
          <?php endif; ?>

          <?php if ($vr->fileSize < 1000 && $vr->fileSize > 0): ?>
            <a href="<?php echo base_url('home/download');?>/<?php echo $vr->records_id; ?>" class="btn btn-primary" title="Download pdf"><span class="glyphicon glyphicon-download"></span> Download (<?php echo number_format($vr->fileSize); ?> KB)</a>
            <span><a href="<?php echo base_url('home/viewPDF');?>/<?php echo $vr->records_id; ?>" target="_blank" class="btn btn-primary" title="Read pdf file"><span class="glyphicon glyphicon-book"></span> Read Online PDF</a></span>
          <?php endif; ?>
        </div>
      </div>
    </div>

  </div>

  <?php endforeach; ?>

  <hr>

  <div class="container">
    <div class="row text-center">
      <a href="<?php echo base_url('home/index'); ?>" class="btn btn-info" style="border-radius: 20px 20px 20px 20px; "><b><span class="glyphicon glyphicon-home"></span>  Go Back Home</b></a>
      <hr style="border:0px;padding-bottom:5px;">
      <p>Copyright 2017 Records Office and Archives Center</p>
      <p class="text-muted">version 1.0</p>
    </div>
  </div>
