<?php

  class StaffModel extends CI_Model  {

    function __construct()  {

      parent::__construct();

    }

    function getStaff() {
      
      $this->db->join('position', 'staff.pos_id = position.pos_id');
      $query = $this->db->get('staff');
      
      if ($query->num_rows() > 0) {
        return $query->result();
      }
      else {
        return $query->result();
      }

    }

    function getSelectedStaff($id) {

      $this->db->join('position', 'staff.pos_id = position.pos_id');
      $this->db->where('staff_id', $id);
      $query = $this->db->get('staff');

      if ($query->num_rows() > 0) {
        return $query->result();
      }
      else {
        return $query->result();
      }
      
    }

    function getPosition()  {

      $this->db->order_by('pos_type', 'asc');
      $query = $this->db->get('position');

      return $query->result();

    }

    function addEmployee($file = array())  {
      
      $data = array(
        'firstname'   => $this->input->post('txtFname'),
        'lastname'    => $this->input->post('txtLname'),
        'middlename'  => $this->input->post('txtMname'),
        'bday'        => $this->input->post('txtBday'),
        'address'     => $this->input->post('txtAddress'),
        'filename'    => $file['file_name'],
        'pos_id'      => $this->input->post('slctPosition')
      );

      $this->db->insert('staff', $data);

    }

    function addEmployeeFailed()  {

      $id = $this->input->post('txtStaffId');

      $data = array(
        'firstname'   => $this->input->post('txtFirstname'),
        'lastname'    => $this->input->post('txtLastname'),
        'middlename'  => $this->input->post('txtMiddlename'),
        'bday'        => $this->input->post('txtBday'),
        'address'     => $this->input->post('txtAddress'),
        'pos_id'      => $this->input->post('slctPosition')
      );

      $this->_update('staff_id', $id, 'staff', $data);

    }

    function updateEmployee($file = array()) {

      $id = $this->input->post('txtStaffId');

      $data = array(
        'firstname'   => $this->input->post('txtFirstname'),
        'lastname'    => $this->input->post('txtLastname'),
        'middlename'  => $this->input->post('txtMiddlename'),
        'bday'        => $this->input->post('txtBday'),
        'address'     => $this->input->post('txtAddress'),
        'filename'    => $file['file_name'],
        'pos_id'      => $this->input->post('slctPosition')
      );

      $this->_update('staff_id', $id, 'staff', $data);

    }

    function _update($where, $id, $table, $data)  {

      $this->db->where($where, $id);
      return $this->db->update($table, $data);

    }
  }

 ?>
