<?php

  class ImageModel extends CI_Model {

    function __construct()  {

      parent::__construct();

    }

    function insertImg($img = array())  {

      $data = array(
        'picGallery_title'  => $this->input->post('txtTitle'),
        'picGallery_desc'   => $this->input->post('txtDescription'),
        'tags_id'           => $this->input->post('slctTag'),
        'filename'          => $img['file_name'],
        'filepath'          => $img['full_path'],
        'filesize'          => $img['file_size']
      );

      $this->db->insert('picgallery', $data);

    }

    function insertImgFailed()  {

      $data = array(
        'picGallery_title'  => $this->input->post('txtTitle'),
        'picGallery_desc'   => $this->input->post('txtDescription'),
        'tags_id'           => $this->input->post('slctTag')
      );

      $this->db->insert('picgallery', $data);

    }

    function updateImageSuccessful($img = array())  {

      $id = $this->input->post('txtImgId');

      $data = array(
        'picGallery_title'  => $this->input->post('txtImgTitle'),
        'picGallery_desc'   => $this->input->post('txtImgDescription'),
        'tags_id'           => $this->input->post('slctImgTags'),
        'filename'          => $img['file_name'],
        'filepath'          => $img['full_path'],
        'filesize'          => $img['file_size']
      );

      $this->db->where('picGallery_id', $id);
      $this->db->update('picgallery', $data);        

    }

    function updateImageFailed()  {

      $id = $this->input->post('txtImgId');

      $data = array(
        'picGallery_title'  => $this->input->post('txtImgTitle'),
        'picGallery_desc'   => $this->input->post('txtImgDescription'),
        'tags_id'           => $this->input->post('slctImgTags')
      );

      $this->db->where('picGallery_id', $id);
      $this->db->update('picgallery', $data);

    }

    function projectImage() {

      $query = $this->db->get('picgallery');

      if ($query->num_rows > 0) {
        return $query->result();
      }
      else {
        return $query->result();
      }

    }

    function getTags()  {

      $this->db->order_by('tags', 'asc');
      $query = $this->db->get('tags');

      if($query->num_rows() > 0) {
        return $query->result();
      }
      else {
        return $query->result();
      }

    }

    function getSelectedImage($id) {

      $this->db->join('tags', 'picgallery.tags_id = tags.tags_id');
      $this->db->where('picGallery_id', $id);

      $query = $this->db->get('picgallery');

      if ($query->num_rows() == 1) {
        return $query->result();
      }
      else {
        return $query->result();
      }

    }

    function getAllImage()  {

      $this->db->join('tags', 'picgallery.tags_id = tags.tags_id');
      $query = $this->db->get('picgallery');

      if ($query->num_rows() > 0) {
        return $query->result();
      }
      else {
        return $query->result();
      }

    }

  } //end of the class

