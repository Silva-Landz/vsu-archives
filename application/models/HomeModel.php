<?php

  class HomeModel extends CI_Model  {

      function __construct()  {

        parent::__construct();

      }

      function getPaginateData($limit, $start, $keyword)  {

        $this->db->select('
          records.records_id,
          records.records_desc,
          records.records_dateField,
          records.filePath,
          records.fileSize,
          category.category_name,
          shelves.shelves_num,
          shelves.shelves_pos'
        );

        $this->db->from('records');
        $this->db->join('category', 'records.category_id = category.category_id');
        $this->db->join('shelves', 'records.shelves_id = shelves.shelves_id');

        $this->db->limit($limit, $start);

        $this->db->or_like(
          array(
            'records.records_desc'      => $keyword, 
            'records.records_dateField' => $keyword,
            'category.category_name'    => $keyword
          )
        );

        $query = $this->db->get();

        if($query->num_rows() > 0)  {
          return $query->result();
        }
        else {
          return $query->result();
        }

      }

      function getData()  {

        $this->db->select('
          records.records_id,
          records.records_desc,
          records.records_dateField,
          records.filePath,
          records.fileSize,
          category.category_name,
          shelves.shelves_num,
          shelves.shelves_pos'
        );

        $this->db->from('records');
        $this->db->join('category', 'records.category_id = category.category_id');
        $this->db->join('shelves', 'records.shelves_id = shelves.shelves_id');

        $query = $this->db->get();

        if($query->num_rows() > 0)  {
          return $query->result();
        }
        else {
          return false;
        }

      }

      function getDataToView($record_id)  {

        $this->db->select('
          records.records_id,
          records.records_desc,
          records.records_dateField,
          records.fileName,
          records.filePath,
          records.fileSize,
          records.pages,
          category.category_name,
          shelves.shelves_num,
          shelves.shelves_pos'
        );

        $this->db->from('records');
        $this->db->join('category', 'records.category_id = category.category_id');
        $this->db->join('shelves', 'records.shelves_id = shelves.shelves_id');
        $this->db->where('records.records_id', $record_id);

        $query = $this->db->get();

        if($query->num_rows() > 0)  {
          return $query->result();
        }
        else {
          return false;
        }

      }

      function getCategory()  {

        $this->db->order_by('category_name', 'ASC');
        $query = $this->db->get('category');

        if ($query->num_rows() > 0) {
          return $query->result();
        }
        else {
          return false;
        }

      }

      function getShelvePosition()  {

        $query = $this->db->get('shelves');

        if ($query->num_rows() > 0) {
          return $query->result();
        }
        else {
          return false;
        }

      }

      function insertRecords($file = array())  {

        $data = array(
          'records_desc'      => $this->input->post('txtDesc'),
          'records_dateField' => $this->input->post('txtDate'),
          'category_id'       => $this->input->post('slctCat'),
          'shelves_id'        => $this->input->post('slctShelfNumAndPos'),
          'fileName'          => $file['file_name'],
          'filePath'          => $file['full_path'],
          'fileSize'          => $file['file_size'],
          'pages'             => $this->input->post('numPages')
        );

        $this->db->insert('records', $data);

      }

      function insertRecordsFailed()  {

        $data = array(
          'records_desc'      => $this->input->post('txtDesc'),
          'records_dateField' => $this->input->post('txtDate'),
          'category_id'       => $this->input->post('slctCat'),
          'shelves_id'        => $this->input->post('slctShelfNumAndPos'),
          'pages'             => $this->input->post('numPages')
        );

        $this->db->insert('records', $data);

      }

      function getEditRecords($record_id) {

        $this->db->join('category', 'records.category_id = category.category_id');
        $this->db->where('records_id', $record_id);

        $query = $this->db->get('records');

        if($query->num_rows() > 0)  {
          return $query->result();
        }
        else {
          return false;
        }

      }

      function updateRec($file = array())  {

        $id = $this->input->post('recID');

        $data = array(
          'records_desc'      => $this->input->post('txtDesc'),
          'records_dateField' => $this->input->post('txtDate'),
          'category_id'       => $this->input->post('slctCat'),
          'shelves_id'        => $this->input->post('slctShelfNumAndPosEdit'),
          'pages'             => $this->input->post('numPages'),
          'fileName'          => $file['file_name'],
          'filePath'          => $file['full_path'],
          'fileSize'          => $file['file_size']
        );

        $this->db->where('records_id', $id);
        $this->db->update('records', $data);

      }

      function updateRecFailed()  {

        $id = $this->input->post('recID');

        $data = array(
          'records_desc'      => $this->input->post('txtDesc'),
          'records_dateField' => $this->input->post('txtDate'),
          'category_id'       => $this->input->post('slctCat'),
          'shelves_id'        => $this->input->post('slctShelfNumAndPosEdit'),
          'pages'             => $this->input->post('numPages')
        );

        $this->db->where('records_id', $id);
        $this->db->update('records', $data);

      }
  }

?>
