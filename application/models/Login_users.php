<?php

  class Login_users extends CI_Model  {

      function __construct()  {

        parent::__construct();

      }

      function can_login()  {

        $this->db->where('username', $this->input->post('txtEmail'));
        $this->db->where('password', md5($this->input->post('txtPass')));

        $query = $this->db->get('users');

        if ($query->num_rows() == 1) {
            return $query->result();
        }
        else {
            return false;
        }
        
      }

      function register_account()   {

        $email = $this->input->post('txtEmail');
        $pass = md5($this->input->post('txtPass'));

        $data = array(
            'username' => $email,
            'password' => $pass
        );

        $this->db->insert('users', $data);

        return true;

      }

  }