-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2018 at 02:29 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `records_office_2016`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`) VALUES
(1, 'Administrative Order'),
(2, 'Annual Report'),
(3, 'Appointment'),
(4, 'Bound 201 files'),
(5, 'Bulletins'),
(6, 'Circulars'),
(7, 'Memorandum'),
(8, 'Memorandum Circulars'),
(9, 'Minutes'),
(10, 'Others'),
(11, 'Reports'),
(12, 'Souvenir Program'),
(13, 'Student Profile'),
(14, 'Annual Development Plan'),
(15, 'Five Year Development Plan'),
(16, 'Other Annual Reports'),
(17, 'Other Memoranda'),
(18, 'Other Issuance'),
(19, 'BOR Minutes'),
(20, 'BOT Minutes'),
(21, 'Institutional Awards'),
(22, 'Converting, Renaming and Integration'),
(23, 'BOR Resolution'),
(24, 'Presidential Decree');

-- --------------------------------------------------------

--
-- Table structure for table `logbook`
--

CREATE TABLE `logbook` (
  `logbook_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `picgallery`
--

CREATE TABLE `picgallery` (
  `picGallery_id` int(11) NOT NULL,
  `picGallery_title` varchar(225) DEFAULT NULL,
  `picGallery_desc` varchar(1000) DEFAULT NULL,
  `tags_id` int(11) DEFAULT NULL,
  `filename` varchar(225) NOT NULL,
  `filepath` varchar(225) NOT NULL,
  `filesize` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `picgallery`
--

INSERT INTO `picgallery` (`picGallery_id`, `picGallery_title`, `picGallery_desc`, `tags_id`, `filename`, `filepath`, `filesize`) VALUES
(3, 'Lorem ipsum', 'Lorem ipsum dolor', 1, 'background-background-image-blue-sky-1054218.jpg', 'C:/xampp/htdocs/vsu-archives/img/archives/background-background-image-blue-sky-1054218.jpg', 1322),
(4, 'Lorem ipsum', 'Donec vel ligula mi. Vestibulum eget lorem pretium, pellentesque est sed, pellentesque lacus. Quisque orci nibh, consectetur at finibus non', 4, '586824451-minimal-wallpaper-hd.jpg', 'C:/xampp/htdocs/vsu-archives/img/archives/586824451-minimal-wallpaper-hd.jpg', 49);

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `pos_id` int(11) NOT NULL,
  `pos_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`pos_id`, `pos_type`) VALUES
(1, 'OIC, Head'),
(2, 'Administrative Aide IV'),
(3, 'Administrative Aide III'),
(4, 'Administrative Aide II'),
(5, 'Web Developer'),
(6, 'Clerk');

-- --------------------------------------------------------

--
-- Table structure for table `records`
--

CREATE TABLE `records` (
  `records_id` int(11) NOT NULL,
  `records_desc` varchar(500) NOT NULL,
  `records_dateField` varchar(100) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `shelves_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT '2',
  `fileName` varchar(200) DEFAULT NULL,
  `filePath` varchar(200) DEFAULT NULL,
  `fileSize` int(11) DEFAULT NULL,
  `pages` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `records`
--

INSERT INTO `records` (`records_id`, `records_desc`, `records_dateField`, `category_id`, `shelves_id`, `staff_id`, `fileName`, `filePath`, `fileSize`, `pages`) VALUES
(4, 'ViSCA Annual Report', '1980', 2, 1, 2, '0', NULL, NULL, 10),
(5, 'ViSCA Annual Report', '1981', 2, 1, 2, '0', NULL, NULL, 0),
(6, 'ViSCA Annual Report', '1982', 2, 1, 2, '0', NULL, NULL, 0),
(8, 'Gen. Circulars (Bureau of Education)', '1924', 6, 2, 2, '0', NULL, NULL, 0),
(9, 'Gen. Circulars (Bureau of Education)', '1923', 6, 2, 2, '0', NULL, NULL, 0),
(10, 'Bulletin No. 1 (Bureau of Public Schools) ', '1951', 5, 2, 2, '0', NULL, NULL, 0),
(11, 'Memorandum (Bureau of Public Schools) ', '1952', 7, 2, 2, '0', NULL, NULL, 0),
(12, 'Circular (Bureau of Public Schools) ', '1958', 6, 6, 2, '', '', NULL, 0),
(13, 'ViSCA Annual Report', '1983', 2, 3, 2, '0', NULL, NULL, 0),
(14, 'ViSCA Annual Report', '1984', 2, 3, 2, '0', NULL, NULL, 0),
(15, 'ViSCA Annual Report', '1985', 2, 3, 2, '0', NULL, NULL, 0),
(16, 'ViSCA Annual Report', '1987', 2, 3, 2, '0', NULL, NULL, 0),
(17, 'ViSCA Annual Report', '1988', 2, 3, 2, '0', NULL, NULL, 0),
(18, 'ViSCA Annual Report', '1989', 2, 3, 2, '0', NULL, NULL, 0),
(19, 'ViSCA Annual Report', '1990', 2, 3, 2, '0', NULL, NULL, 0),
(20, 'ViSCA Annual Report', '1991', 2, 3, 2, '0', NULL, NULL, 0),
(21, 'ViSCA Annual Report', '1992', 2, 3, 2, '0', NULL, NULL, 0),
(22, 'Circular, Memos and Bulletins (Bureau of Vocational Education)', '1965', 10, 4, 2, '0', NULL, NULL, 0),
(23, 'Circulars (BNAS) ', '1958', 6, 4, 2, '0', NULL, NULL, 0),
(24, 'Bulletins (Bureau of Public Schools)', '1958', 5, 4, 2, '0', NULL, NULL, 0),
(25, 'Bulletins (Bureau of Public Schools)', '1957', 5, 4, 2, '0', NULL, NULL, 0),
(26, 'Circulars and Memos (Bureau of Public Schools)', '1963', 10, 4, 2, '0', NULL, NULL, 0),
(27, 'ViSCA Annual Report', '1993', 2, 5, 2, '0', NULL, NULL, 0),
(28, 'ViSCA Annual Report', '1994', 2, 5, 2, '0', NULL, NULL, 0),
(29, 'ViSCA Annual Report', '1995', 2, 5, 2, '0', NULL, NULL, 0),
(30, 'ViSCA Annual Report', '1996', 2, 5, 2, '0', NULL, NULL, 0),
(31, 'ViSCA Annual Report', '1997', 2, 5, 2, '0', NULL, NULL, 0),
(32, 'ViSCA Annual Report', '1998', 2, 5, 2, '0', NULL, NULL, 0),
(33, 'ViSCA Annual Report', '1999', 2, 5, 2, '0', NULL, NULL, 0),
(34, 'ViSCA Annual Report', '2000', 2, 5, 2, '0', NULL, NULL, 0),
(35, 'ViSCA Annual Report', '2001', 2, 5, 2, '0', NULL, NULL, 0),
(36, 'ViSCA Annual Report', '2002', 2, 5, 2, '0', NULL, NULL, 0),
(37, 'ViSCA Annual Report', '2003', 2, 5, 2, '0', NULL, NULL, 0),
(38, 'Delfin D. Zamar', NULL, 4, 6, 2, '0', NULL, NULL, 0),
(39, 'Julian M. Mamril', NULL, 4, 6, 2, '0', NULL, NULL, 0),
(40, 'Dominador D. Celemente', NULL, 4, 6, 2, '0', NULL, NULL, 0),
(41, 'Rafael G. Mamaril', NULL, 4, 6, 2, '0', NULL, NULL, 0),
(42, 'Annual Report', '2004', 2, 7, 2, '0', NULL, NULL, 0),
(43, 'Annual Report', '2005', 2, 7, 2, '0', NULL, NULL, 0),
(44, 'Annual Report', '2006', 2, 7, 2, '0', NULL, NULL, 0),
(45, 'Annual Report', '2007', 2, 7, 2, '0', NULL, NULL, 0),
(46, 'Annual Report', '2008', 2, 7, 2, '0', NULL, NULL, 0),
(47, 'Annual Report', '2009', 2, 7, 2, '0', NULL, NULL, 0),
(48, 'Annual Report', '2010', 2, 7, 2, '0', NULL, NULL, 0),
(49, 'Annual Report', '2011', 2, 7, 2, '0', NULL, NULL, 0),
(50, 'Annual Report', '2012', 2, 7, 2, '0', NULL, NULL, 0),
(51, 'Annual Report', '2013', 2, 7, 2, '0', NULL, NULL, 0),
(52, 'Vicente M. Saavedra', NULL, 4, 8, 2, '0', NULL, NULL, 0),
(53, 'Fernando A. Bernardo', NULL, 4, 8, 2, '0', NULL, NULL, 0),
(54, 'Marianito R. Villanueva', NULL, 4, 8, 2, '0', NULL, NULL, 0),
(55, 'Samuel S. Go', NULL, 4, 8, 2, '0', NULL, NULL, 0),
(56, 'Annual Report(High School Department)', '1971-1972', 2, 9, 2, '0', NULL, NULL, 0),
(57, 'Annual Report(PhilRootcrops)', '1977', 2, 9, 2, '0', NULL, NULL, 0),
(58, 'Annual Report(PhilRootcrops)', '1976-1977', 2, 9, 2, '0', NULL, NULL, 0),
(59, 'Annual Report(PhilRootcrops) Vol. II', '1979 ', 2, 9, 2, '0', NULL, NULL, 0),
(60, 'Annual Report(RTC-RD)', '1981', 2, 9, 2, '0', NULL, NULL, 0),
(61, 'Graduate Catalog', '1994-1995', 10, 10, 2, '0', NULL, NULL, 0),
(62, 'Profile Update', '2003', 10, 10, 2, '0', NULL, NULL, 0),
(63, 'ViSCA Faculty Profile Statistical Report', '1972', 11, 10, 2, '0', NULL, NULL, 0),
(64, 'ViSCA Faculty Profile Statistical Report', '1994', 11, 10, 2, '0', NULL, NULL, 0),
(65, 'ViSCA Faculty Profile Statistical Report', '1995', 11, 10, 2, '0', NULL, NULL, 0),
(66, 'ViSCA Faculty Profile Statistical Report', '1999', 11, 10, 2, '0', NULL, NULL, 0),
(67, 'ViSCA Faculty Profile Statistical Report', '2000', 11, 10, 2, '0', NULL, NULL, 0),
(68, 'ViSCA Faculty Profile Statistical Report', '2002', 11, 10, 2, '0', NULL, NULL, 0),
(69, 'ViSCA Faculty Profile Statistical Report', '2004', 11, 10, 2, '0', NULL, NULL, 0),
(70, 'Terminal Report (PCARDD-PRCRTC funded Research Project)', NULL, 11, 10, 2, '0', NULL, NULL, 0),
(71, 'ViSCA Student Profile Vol. 2 No. 11, 2nd and 1st Sem.', '1985-1991, 1996-1997', 13, 10, 2, '0', NULL, NULL, 0),
(72, 'ViSCA Student Profile Vol. 2 No. 9, 1st, Summer, and 2nd Sem.', '1994-1995, 1994, 1993-1994', 13, 10, 2, '0', NULL, NULL, 0),
(73, 'ViSCA Student Profile Vol. 2 No. 3, 1st and 2nd Sem.', '1988-1989', 13, 10, 2, '0', NULL, NULL, 0),
(74, 'ViSCA Student Profile Vol. 2 No. 13, 2nd and 1st Sem. ', '1997-1998, 1998-1999', 13, 10, 2, '0', NULL, NULL, 0),
(75, 'ViSCA Student Profile Vol. 2 No. 10, 1st and 2nd Sem. ', '1995-1996, 1994-1995', 13, 10, 2, '0', NULL, NULL, 0),
(76, 'ViSCA Student Profile Vol. 2 No. 11, 2nd Sem. ', '1995-1996, 1996-1997', 13, 10, 2, '0', NULL, NULL, 0),
(77, 'ViSCA Student Profile Vol. 2 No. 7, 2nd and 1st Sem. ', '1991-1992, 1992-1993', 13, 10, 2, '0', NULL, NULL, 0),
(78, 'ViSCA Student Profile Vol. 2 No. 8, 1st and 2nd Sem. ', '1993-1994, 1992-1993', 13, 10, 2, '0', NULL, NULL, 0),
(79, 'ViSCA Student Profile Vol. 2 No. 14, 2nd and 1st Sem. ', '1998-1999, 1999-2000', 13, 10, 2, '0', NULL, NULL, 0),
(80, '55th ViSCA Anniversary', 'Aug. 24-Sept. 1979', 12, 11, 2, '0', NULL, NULL, 0),
(81, '78th LSU Anniversary', 'August 5-11, 2002', 12, 11, 2, '0', NULL, NULL, 0),
(82, '80th LSU Anniversary', 'August 2-11, 2004', 12, 11, 2, '0', NULL, NULL, 0),
(83, '82nd VSU Anniversary', 'August 5-11, 2006', 12, 11, 2, '0', NULL, NULL, 0),
(84, '83rd VSU Anniversary', 'August 10-11, 2007', 12, 11, 2, '0', NULL, NULL, 0),
(85, '84th VSU Anniversary', 'August 10-11, 2008', 12, 11, 2, '0', NULL, NULL, 0),
(86, '85th VSU Anniversary', 'August 10-11, 2009', 12, 11, 2, '0', NULL, NULL, 0),
(87, '86th VSU Anniversary', 'August 10-11, 2010', 12, 11, 2, '0', NULL, NULL, 0),
(88, '87th VSU Anniversary', 'August 11, 2011', 12, 11, 2, '0', NULL, NULL, 0),
(89, '88th VSU Anniversary', 'August 11, 2012', 12, 11, 2, '0', NULL, NULL, 0),
(90, '89th VSU Anniversary', 'August 11, 2013', 12, 11, 2, '0', NULL, NULL, 0),
(91, 'ACCU Publication(Strengthening the Linkage between Research 7 Extension)', NULL, 10, 12, 2, '0', NULL, NULL, 0),
(92, 'ACCU Publication(The Role of Agriculture Colleges and Universities in Small Farmer Education)', NULL, 10, 12, 2, '0', NULL, NULL, 0),
(93, 'LSU Gazettes Vol. 1 Issue #3 (Blazing the Trail in Environmental Conversation and Management)', 'December 2002', 10, 12, 2, '0', NULL, NULL, 0),
(94, 'ViSCA History and Analysis of Institution Building by F.A. Bernardo', NULL, 10, 12, 2, '0', NULL, NULL, 0),
(95, 'ViSCA Code', NULL, 10, 12, 2, '0', NULL, NULL, 0),
(96, 'Records Manual', NULL, 10, 12, 2, '0', NULL, NULL, 0),
(97, 'LSU BOR Minutes – Nos. 15th – 22nd', 'Mar. 2004 – December 2005', 9, 13, 2, '0', NULL, NULL, 0),
(98, 'LSU BOR Minutes – Nos. 9th – 10th', 'February – May 2003', 9, 13, 2, '0', NULL, NULL, 0),
(99, 'LSU BOR Minutes – Nos. 11th – 14th', 'June – December 2003', 9, 13, 2, '0', NULL, NULL, 0),
(100, 'LSU BOR Minutes – Nos. 1st – 3rd', 'September 2001 – February 8, 2', 9, 13, 2, '0', NULL, NULL, 0),
(101, 'LSU BOR Minutes – Nos. 4th – 8th', 'April – December 2002', 9, 13, 2, '0', NULL, NULL, 0),
(102, 'ViSCA BOT Minutes – Nos. 43rd – 48th', 'November – July 1981-1982', 9, 14, 2, '0', NULL, NULL, 0),
(103, 'ViSCA BOT Minutes – Nos. 49nth – 58th', 'October – March 1982-1984', 9, 14, 2, '0', NULL, NULL, 0),
(104, 'ViSCA BOT Minutes – Nos. 59nth – 66th', 'April – May 1984-1985', 9, 14, 2, '0', NULL, NULL, 0),
(105, 'ViSCA BOT Minutes – Nos. 161st – 165th', 'January – July 2001', 9, 14, 2, '0', NULL, NULL, 0),
(106, 'ViSCA BOT Minutes – Nos. 137th – 142nd', '1997', 9, 14, 2, '0', NULL, NULL, 0),
(107, 'ViSCA BOT Minutes – Nos. 16th – 28th', '1977-1978', 9, 15, 2, '0', NULL, NULL, 0),
(108, 'ViSCA BOT Minutes – Nos. 1st – 15th', '1974-1976', 9, 15, 2, '0', NULL, NULL, 0),
(109, 'ViSCA BOT Minutes – Nos. 35th – 42nd', 'July – September 1980-1981', 9, 15, 2, '0', NULL, NULL, 0),
(110, 'ViSCA BOT Minutes – Nos. 155th – 160th', 'March – October 2000', 9, 15, 2, '0', NULL, NULL, 0),
(111, 'BOR Meeting ', '2001', 10, 15, 2, '0', NULL, NULL, 0),
(112, 'React-ViCARP-Brochure #1 – “Utilization of Rootcrops for Animal Feeds”', NULL, 10, 16, 2, '0', NULL, NULL, 0),
(113, 'Sweet Potato Cookbook, Vol. 1 “Rootcrop Processing and Utilization”', '', 10, 10, 2, '', 'D:/RECORDS OFFICE FILES/Records Uploaded/', NULL, 0),
(114, 'CSSP The Philippine Journal of Crop Science', NULL, 10, 16, 2, '0', NULL, NULL, 0),
(115, 'Socio-Economic Profile ', '1997', 10, 16, 2, '0', NULL, NULL, 0),
(116, 'Facts and Figures ', '2006', 10, 16, 2, '0', NULL, NULL, 0),
(117, 'Facts and Figures ', '2007', 10, 16, 2, '0', NULL, NULL, 0),
(118, 'Facts and Figures ', '1997', 10, 16, 2, '0', NULL, NULL, 0),
(119, 'Brochure “Molding Dynamic Leaders in Agriculture and Allied Fields”', NULL, 10, 16, 2, '0', NULL, NULL, 0),
(120, 'Amaranth, Vol. 6.2008.2, Second Semester', '2007-2008', 10, 16, 2, '0', NULL, NULL, 0),
(121, 'Proceedings of “First Annual Review and Planning Workshop for Administration and Support Services', 'August 28-29, 1996', 10, 16, 2, '0', NULL, NULL, 0),
(122, 'Minutes of the ADCO Meeting ', 'January 2001', 9, 16, 2, '0', NULL, NULL, 0),
(123, 'Minutes of Meetings of Academic Personnel Board', NULL, 9, 16, 2, '0', NULL, NULL, 0),
(124, 'Arts and Letters Dept. & Forestry Dept.', '1975-1980', 3, 17, 2, '0', NULL, NULL, 0),
(125, 'Home Science, Physical Education, Plant Breeding', '1975-1980', 3, 17, 2, '0', NULL, NULL, 0),
(126, 'IGPO, Records, Security, SPMD', '1975-1980', 3, 17, 2, '0', NULL, NULL, 0),
(127, 'Accounting, Cash, OVPA, Legal and Personnel Office', '1975-1980', 3, 17, 2, '0', NULL, NULL, 0),
(128, 'CEVARC, VRC, IGPO', '1977-1979, 1983-1984, 1981-1985', 3, 17, 2, '0', NULL, NULL, 0),
(129, 'PCARR Project No. 279, PCARR Project No. 335, PCARR Project No. 721', '1976-1978, 1976-1981, 1978-1979', 3, 17, 2, '0', NULL, NULL, 0),
(130, 'PPO ', '1978-1979', 3, 17, 2, '0', NULL, NULL, 0),
(131, 'PPO', '1978-1979', 3, 17, 2, '0', NULL, NULL, 0),
(132, 'Accounting, Cash, Personnel, Security', '1981-1985', 3, 17, 2, '0', NULL, NULL, 0),
(133, 'PPO Repairs and Maintenance ', '1981', 3, 17, 2, '0', NULL, NULL, 0),
(134, 'PPO Repairs and Maintenance ', '1982', 3, 17, 2, '0', NULL, NULL, 0),
(135, 'PPO Repairs and Maintenance ', '1983', 3, 17, 2, '0', NULL, NULL, 0),
(136, 'PPO Repairs and Maintenance ', '1984', 3, 17, 2, '0', NULL, NULL, 0),
(137, 'PPO ', '1985', 3, 17, 2, '0', NULL, NULL, 0),
(138, 'PPO ', '1986', 3, 17, 2, '0', NULL, NULL, 0),
(139, 'OVPA, CSR, RCRC, DPP, OCS, OGS, Budget Office, VGH, PPO', '1986-1990, 1986-1990, 1986-1990, 1984-1990, 1986-1990, 1986-1990,1986-1990', 3, 17, 2, '0', NULL, NULL, 0),
(140, 'DHS , VGH, DPBAB, DPE ', '1981-1985', 3, 17, 2, '0', NULL, NULL, 0),
(141, 'COA, Budget Office, SPMD', '1981-1982, 1981-1985, 1981-1985', 3, 17, 2, '0', NULL, NULL, 0),
(142, 'EDPC, OGS, Plant Protection', '1984-1985, 1983-1985, 1981-1985', 3, 17, 2, '0', NULL, NULL, 0),
(143, 'PRCRTC Project No. 001', '1978-1982', 3, 17, 2, '0', NULL, NULL, 0),
(144, 'PRCRTC Project No. 002', '1978-1980', 3, 17, 2, '0', NULL, NULL, 0),
(145, 'PRCRTC Project No. 004', '1978-1979', 3, 17, 2, '0', NULL, NULL, 0),
(146, 'PRCRTC Project No. 005', '1979-1982', 3, 17, 2, '0', NULL, NULL, 0),
(147, 'PRCRTC Project No. 006', '1979-1982', 3, 17, 2, '0', NULL, NULL, 0),
(148, 'PRCRTC Project No. 007', '1979-1980', 3, 17, 2, '0', NULL, NULL, 0),
(149, 'PRCRTC Project No. 008', '1979-1981', 3, 17, 2, '0', NULL, NULL, 0),
(150, 'PRCRTC Project No. 009', '1979-1981', 3, 17, 2, '0', NULL, NULL, 0),
(151, 'PRCRTC Project No. 010', '1979-1983', 3, 17, 2, '0', NULL, NULL, 0),
(152, 'PRCRTC Project No. 011', '1979-1982', 3, 17, 2, '0', NULL, NULL, 0),
(153, 'PRCRTC Project No. 012', '1979-1981', 3, 17, 2, '0', NULL, NULL, 0),
(154, 'PRCRTC Project No. 013', '1979-1981', 3, 17, 2, '0', NULL, NULL, 0),
(155, 'PRCRTC Project No. 014', '1979-1981', 3, 17, 2, '0', NULL, NULL, 0),
(156, 'PRCRTC Project No. 015', '1979-1980', 3, 17, 2, '0', NULL, NULL, 0),
(157, 'PRCRTC Project No. 016', '1979-1983', 3, 17, 2, '0', NULL, NULL, 0),
(158, 'PRCRTC Project No. 017', '1979-1984', 3, 17, 2, '0', NULL, NULL, 0),
(159, 'PRCRTC Project No. 018', '1980-1982', 3, 17, 2, '0', NULL, NULL, 0),
(160, 'PRCRTC Project No. 020', '1980-1982', 3, 17, 2, '0', NULL, NULL, 0),
(161, 'PRCRTC Project No. 047', '1980-1984', 3, 17, 2, '0', NULL, NULL, 0),
(162, 'PRCRTC Project No. 050', '1980-1984', 3, 17, 2, '0', NULL, NULL, 0),
(163, 'PRCRTC Project No. 051', '1980-1984', 3, 17, 2, '0', NULL, NULL, 0),
(164, 'PRCRTC Project No. 052', '1980-1983', 3, 17, 2, '0', NULL, NULL, 0),
(165, 'PRCRTC Project No. 053', '1980-1982', 3, 17, 2, '0', NULL, NULL, 0),
(166, 'PRCRTC Project No. 054', '1979-1981', 3, 17, 2, '0', NULL, NULL, 0),
(167, 'PRCRTC Project No. 055', '1981', 3, 17, 2, '0', NULL, NULL, 0),
(168, 'PRCRTC Project No. 056', '1981-1984', 3, 17, 2, '0', NULL, NULL, 0),
(169, 'PRCRTC Project No. 058', '1981', 3, 17, 2, '0', NULL, NULL, 0),
(170, 'PRCRTC Project No. 059', '1981', 3, 17, 2, '0', NULL, NULL, 0),
(171, 'PRCRTC Project No. 061', '1981-1982', 3, 17, 2, '0', NULL, NULL, 0),
(172, 'PRCRTC Project No. 067', '1982', 3, 17, 2, '0', NULL, NULL, 0),
(173, 'PRCRTC Project No. 073', '1982-1983', 3, 17, 2, '0', NULL, NULL, 0),
(174, 'PRCRTC Project No. 099', '1984-1985', 3, 17, 2, '0', NULL, NULL, 0),
(175, 'PRCRTC Project No. 662', '1978-1980', 3, 17, 2, '0', NULL, NULL, 0),
(176, 'DYAC, PRCRTC, DASVM', '1986-1990, 1986-1990, 1986-1990', 3, 17, 2, '0', NULL, NULL, 0),
(177, 'PRCRTC, RCRC', '1981-1985', 3, 17, 2, '0', NULL, NULL, 0),
(178, 'PARRS Project No. 5200-79.00.001, PARRS Project No. 499', '1980-1985, 1979-1984', 3, 17, 2, '0', NULL, NULL, 0),
(179, 'ERHS', '1981-1985', 3, 17, 2, '0', NULL, NULL, 0),
(180, 'PIU-EDPITAF', '1981-1982', 3, 17, 2, '0', NULL, NULL, 0),
(181, 'Auditor, Infirmary, Library, Student Affairs', '1975-1980', 3, 17, 2, '0', NULL, NULL, 0),
(182, 'PIU-EDPITAF', '1977-1980', 3, 17, 2, '0', NULL, NULL, 0),
(183, 'ERHS ', '1975-1980', 3, 17, 2, '0', NULL, NULL, 0),
(184, 'OMO, OAA, legal, Records, OBAA, VMO, Office of the Registrar, Mimeo and Janitorial Secretary, OVPAA', '1984-1986, 1982-1986, 1981, 1981-1982, 1981-1982, 1981-1985, 1981-1985, 1981-1985, 1981-1985  ', 3, 17, 2, '0', NULL, NULL, 0),
(185, 'RCRC, PRCRTC', '1976-1980', 3, 17, 2, '0', NULL, NULL, 0),
(186, 'Ag. Chem., ADE, DASS', '1975-1980', 3, 17, 2, '0', NULL, NULL, 0),
(187, 'DAEAM, ATR, Horticulture, DYAC, Infirmary', '1981-1985, 1980-1985, 1982-1985, 1982-1985, 1981-1985', 3, 17, 2, '0', NULL, NULL, 0),
(188, 'Appt. of Regular Employees ', '1990', 3, 17, 2, '0', NULL, NULL, 0),
(189, 'Appt. of Casual Employees (DOF, IGPO, ERHS, VMO, Infirmary, DPE, DAEF, DDC)', '1986-1990', 3, 17, 2, '0', NULL, NULL, 0),
(190, 'Appt. of Casual Employees (SPMD, VSD, PO, OP, OSSA, Accounting Division)', '1986-1990', 3, 17, 2, '0', NULL, NULL, 0),
(191, 'Memorandum ', 'January 5, 2009-December 21, 2009', 7, 18, 2, '0', NULL, NULL, 0),
(192, 'Memorandum ', 'Series of 2010', 7, 18, 2, '0', NULL, NULL, 0),
(193, 'Memorandum ', 'Series of 2011', 7, 18, 2, '0', NULL, NULL, 0),
(194, 'Memorandum ', 'Series of 2012', 7, 18, 2, '0', NULL, NULL, 0),
(195, 'Memorandum ', 'Series of 2013', 7, 18, 2, '0', NULL, NULL, 0),
(196, 'Memorandum ', 'Series of 2014', 7, 18, 2, '0', NULL, NULL, 0),
(197, 'Memorandum ', 'January 2002-December 2002', 7, 19, 2, '0', NULL, NULL, 0),
(198, 'Memorandum ', 'January 3, 2000-December 18, 2001', 7, 19, 2, '0', NULL, NULL, 0),
(199, 'Memorandum ', 'January-December 2004', 7, 19, 2, '0', NULL, NULL, 0),
(200, 'Memorandum ', 'January-December 2005', 7, 19, 2, '0', NULL, NULL, 0),
(201, 'Memorandum ', 'January-December 2006', 7, 19, 2, '0', NULL, NULL, 0),
(202, 'Memorandum ', 'January-December 2007', 7, 19, 2, '0', NULL, NULL, 0),
(203, 'Memorandum ', 'January-December 2008', 7, 19, 2, '0', NULL, NULL, 0),
(204, 'Memorandum Circular ', '1987-1989, 1999, 2000,-2001', 8, 20, 2, '0', NULL, NULL, 0),
(205, 'Memorandum Circular ', '1975-1980', 8, 20, 2, '0', NULL, NULL, 0),
(206, 'Memorandum ', '1983-1984', 7, 20, 2, '0', NULL, NULL, 0),
(207, 'Memorandum Circular ', 'January 2002-December 2002', 8, 20, 2, '0', NULL, NULL, 0),
(208, 'Memorandum Circular ', 'January 2003-December 2003', 8, 20, 2, '0', NULL, NULL, 0),
(209, 'Memorandum Circular Nos. 1-66', '2004-2005', 8, 20, 2, '0', NULL, NULL, 0),
(210, 'Memorandum Circular Nos. 1-50', 'January-December 2006', 8, 20, 2, '0', NULL, NULL, 0),
(211, 'Memorandum Circular Nos. 1-69', 'January-December 2007', 8, 20, 2, '0', NULL, NULL, 0),
(212, 'Memorandum Circular Nos. 1-68', 'January-December 2008', 8, 20, 2, '0', NULL, NULL, 0),
(213, 'Memorandum Circular', 'January 15, 2009-December 15, 2009', 8, 20, 2, '0', NULL, NULL, 0),
(214, 'OP Memorandum Circular', '2011-2012', 8, 20, 2, '0', NULL, NULL, 0),
(215, 'Memorandum Circular', '2011-2012', 8, 20, 2, '0', NULL, NULL, 0),
(216, 'OP Memorandum Circular', 'Series of 2013', 8, 20, 2, '0', NULL, NULL, 0),
(217, 'Memorandum', '1986-2000', 7, 20, 2, '0', NULL, NULL, 0),
(218, 'Memorandum', '1992-1993', 7, 20, 2, '0', NULL, NULL, 0),
(219, 'Memorandum', '1994-1996', 7, 20, 2, '0', NULL, NULL, 0),
(220, 'Memorandum', '1988-1989', 7, 20, 2, '0', NULL, NULL, 0),
(221, 'Memorandum', '1997-1999', 7, 20, 2, '0', NULL, NULL, 0),
(222, 'Memorandum', '1984-1985', 7, 20, 2, '0', NULL, NULL, 0),
(223, 'Memorandum', '1986-1987', 7, 20, 2, '0', NULL, NULL, 0),
(224, 'Memorandum', '1990-1991', 7, 20, 2, '0', NULL, NULL, 0),
(225, 'Memorandum', '1982-1983', 7, 20, 2, '0', NULL, NULL, 0),
(226, 'Memorandum (OBAA)', '1982-1981', 7, 20, 2, '0', NULL, NULL, 0),
(227, 'Memorandum', '1978-1979', 7, 20, 2, '0', NULL, NULL, 0),
(228, 'Memorandum (Vice-Pres.)', '1979-1981', 7, 20, 2, '0', NULL, NULL, 0),
(229, 'Memorandum', '1978-1979', 7, 20, 2, '0', NULL, NULL, 0),
(230, 'Memorandum (OBAA)', '1975-1979', 7, 20, 2, '0', NULL, NULL, 0),
(231, 'Memoranda', '1974-1977', 7, 20, 2, '0', NULL, NULL, 0),
(232, 'Administrative Order ', '1982-1990', 1, 1, 2, '', '', NULL, 0),
(235, 'VSU Obelisk ', '2010', 10, 20, 2, '0', NULL, NULL, 0),
(236, 'VSU Obelisk ', '2011', 10, 20, 2, '0', NULL, NULL, 0),
(237, 'VSU Obelisk ', '2012', 10, 20, 2, '0', NULL, NULL, 0),
(238, 'VSU Obelisk ', '2013', 10, 20, 2, '0', NULL, NULL, 0),
(239, 'VSU Obelisk ', '2014', 10, 20, 2, '0', NULL, NULL, 0),
(240, 'Converting the Visayas Agricultural College into the Visayas State College of Agriculture under P. D. 470  and Amended under P. D. 700', '', 10, 10, 2, '', '', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `shelves`
--

CREATE TABLE `shelves` (
  `shelves_id` int(11) NOT NULL,
  `shelves_num` int(11) NOT NULL,
  `shelves_pos` enum('Upper','Middle','Lower','Glass Table') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shelves`
--

INSERT INTO `shelves` (`shelves_id`, `shelves_num`, `shelves_pos`) VALUES
(1, 1, 'Upper'),
(2, 1, 'Lower'),
(3, 2, 'Upper'),
(4, 2, 'Lower'),
(5, 3, 'Upper'),
(6, 3, 'Lower'),
(7, 4, 'Upper'),
(8, 4, 'Lower'),
(9, 5, 'Upper'),
(10, 5, 'Lower'),
(11, 6, 'Upper'),
(12, 6, 'Lower'),
(13, 7, 'Upper'),
(14, 7, 'Middle'),
(15, 7, 'Lower'),
(16, 8, 'Upper'),
(17, 8, 'Lower'),
(18, 9, 'Upper'),
(19, 9, 'Middle'),
(20, 9, 'Lower'),
(21, 1, 'Glass Table'),
(22, 2, 'Glass Table');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `bday` date DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `filename` varchar(225) NOT NULL,
  `image_path` varchar(225) DEFAULT NULL,
  `pos_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `firstname`, `lastname`, `middlename`, `bday`, `address`, `filename`, `image_path`, `pos_id`) VALUES
(1, 'Asteria', 'Sevilla', 'A', '1957-05-20', 'Tinag-an, Albuera, Leyte', 'asteria.jpg', NULL, 1),
(2, 'Graciana', 'Espinosa', 'Managbanag', '1959-12-19', 'National Highway, Brgy. Patag, Baybay City, Leyte', 'graciana.jpg', NULL, 2),
(3, 'Virgilio', 'Acilo', 'Cajeric', '1962-03-04', 'Bunga, Baybay City, Leyte', 'vergelio.jpg', NULL, 3),
(4, 'Alex', 'Bagarinao', 'Posas', '2017-04-05', 'Marcos, Baybay City, Leyte', 'alex.jpg', NULL, 4),
(7, 'Landrex', 'Rebuera', 'Orozco', '1995-02-03', 'Madrid, Surigao del sur', 'LandrexEdited.jpg', NULL, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `tags_id` int(11) NOT NULL,
  `tags` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tags_id`, `tags`) VALUES
(1, 'Buildings'),
(2, 'Events'),
(3, 'Dormitories and Cottages'),
(4, 'Past Administrators'),
(5, 'Presidents'),
(6, 'Vice Presidents'),
(7, 'Sceneries');

-- --------------------------------------------------------

--
-- Table structure for table `temp_users`
--

CREATE TABLE `temp_users` (
  `tmpID` int(11) NOT NULL,
  `tmpEmail` varchar(255) NOT NULL,
  `tmpKey` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `usersID` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`usersID`, `username`, `password`) VALUES
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `logbook`
--
ALTER TABLE `logbook`
  ADD PRIMARY KEY (`logbook_id`);

--
-- Indexes for table `picgallery`
--
ALTER TABLE `picgallery`
  ADD PRIMARY KEY (`picGallery_id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `records`
--
ALTER TABLE `records`
  ADD PRIMARY KEY (`records_id`);

--
-- Indexes for table `shelves`
--
ALTER TABLE `shelves`
  ADD PRIMARY KEY (`shelves_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tags_id`);

--
-- Indexes for table `temp_users`
--
ALTER TABLE `temp_users`
  ADD PRIMARY KEY (`tmpID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`usersID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `logbook`
--
ALTER TABLE `logbook`
  MODIFY `logbook_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `picgallery`
--
ALTER TABLE `picgallery`
  MODIFY `picGallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `pos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `records`
--
ALTER TABLE `records`
  MODIFY `records_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;

--
-- AUTO_INCREMENT for table `shelves`
--
ALTER TABLE `shelves`
  MODIFY `shelves_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tags_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `usersID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
